import { createServer } from '../lib/server'

createServer()
  .then((server) => {
    server.start()
    console.log(`Server running at: ${server.info.uri}`)
    console.log(`process.env.NODE_ENV: ${process.env.NODE_ENV}`)
  })
  .catch((err) => {
    console.error(err)
    process.exit(1)
  })
