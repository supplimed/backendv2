import { authenticationPlugin } from './infrastructure/application/authentication'
import { productsPlugin } from './infrastructure/application/products'
import { healthcheckPlugin } from './infrastructure/application/healthcheck'
import { usersPlugin } from './infrastructure/application/users'
import { orderPlugin } from './infrastructure/application/orders'
import { testPlugin } from './infrastructure/application/test'
import { paymentPlugin } from './infrastructure/application/payment'

const authenticatedPlugins = [
  usersPlugin,
  orderPlugin,
  paymentPlugin
]

const unauthenticatedPlugins = [
  healthcheckPlugin,
  productsPlugin
]

export default {
  unauthenticatedPlugins,
  authenticationPlugin,
  authenticatedPlugins,
  testPlugin
}
