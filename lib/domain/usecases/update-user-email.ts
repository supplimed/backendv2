import * as securityService from '../services/security-service'
import { User } from '../models/User'

import {
  ForbiddenActionError
} from '../errors'

export async function updateUserEmail ({
  userId,
  updatedUserId,
  newEmail,
  newEmailConfirmation,
  userRepository
}): Promise<User> {
  if (userId !== updatedUserId) {
    throw new ForbiddenActionError()
  }
  // TODO check email pas déjà pris
  const userDomain = await userRepository.getUserByIdFilesystem(userId)
  userDomain.updateUserEmail(newEmail, newEmailConfirmation, { securityService })

  return userRepository.updateUser(userDomain)
}
