import { NotFoundError, OrderInvalidError } from '../errors'
import { OrderHistory } from '../models/OrderHistory'
import { DatabaseHistoryOrderHistory } from '../../../db/filesystem/models/history/DatabaseHistoryOrderHistory'

// Compare OrderHistory by date (newest to oldest)
function compare (a: OrderHistory, b: OrderHistory): number {
  if (a.date < b.date) {
    return 1
  }
  if (a.date > b.date) {
    return -1
  }
  return 0
}

export async function getUserOrderHistory ({
  id,
  orderRepository
}): Promise<OrderHistory[]> {
  let databaseOrderHistoryHistory: DatabaseHistoryOrderHistory[]

  try {
    databaseOrderHistoryHistory = await orderRepository.findOrderHistoryByUserIdFilesystem(id)
  } catch (err) {
    if (err instanceof NotFoundError) {
      // If no history is found,  return an empty array
      return []
    }
    throw new OrderInvalidError()
  }

  // Check if it's not empty

  if (databaseOrderHistoryHistory.length < 1) throw new OrderInvalidError()

  // Transform in an OrderHistory array

  const orderHistory: OrderHistory[] = []

  databaseOrderHistoryHistory.forEach(element => orderHistory.push(OrderHistory.fromDatabaseOrderHistoryHistory(element)))

  // Sort OrderHistory newest to oldest

  orderHistory.sort(compare)

  return orderHistory
}
