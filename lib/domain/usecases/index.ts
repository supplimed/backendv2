import * as _ from 'lodash'
import userRepository from '../../infrastructure/repositories/user-repository'
import productRepository from '../../infrastructure/repositories/product-repository'
import orderRepository from '../../infrastructure/repositories/order-repository'
import testRepository from '../../infrastructure/repositories/test-repository'
import { addProductsToBasket } from './add-products-basket'
import { deleteProductsFromBasket } from './delete-products-basket'
import { logUserIn } from './log-user-in'
import { findProducts } from './find-products'
import { findProductById } from './find-product'
import { getUserBasket } from './get-user-basket'
import { getCurrentUser } from './get-current-user'
import { setProductQuantity } from './set-product-quantity'
import { signupUser } from './signup-user'
import { updateUserPassword } from './update-user-password'
import { updateUserEmail } from './update-user-email'
import { resetDb } from './reset-db'
import { createLegalUser } from './mp-create-legal-user'
import { createWallet } from './mp-create-wallet'
import { createCardRegistration } from './mp-create-card-registration'
import { saveCardRegistration } from './mp-save-card-registration'
import { payInCardDirect } from './mp-payin-card-direct'
import { getUserCards } from './mp-get-user-cards'
import { updateOrderAddress } from './update-order-address'
import { getUserOrder } from './get-user-order'
import { addUserAddress } from './add-user-address'
import { historyAddOrder } from './history-add-order'
import { getUserOrderHistory } from './get-user-order-history'
import { getUserOrderHistoryById } from './get-user-order-history-by-id'

function injectDefaults (defaults, targetFn) {
  return (args) => targetFn(Object.assign(Object.create(defaults), args))
}

const dependencies = {
  userRepository: userRepository,
  productRepository: productRepository,
  orderRepository: orderRepository,
  testRepository: testRepository
}

function injectDependencies (usecases) {
  return _.mapValues(usecases, _.partial(injectDefaults, dependencies))
}

export default (injectDependencies({
  logUserIn,
  signupUser,
  updateUserPassword,
  updateUserEmail,
  getCurrentUser,
  findProducts,
  findProductById,
  getUserBasket,
  addProductsToBasket,
  setProductQuantity,
  deleteProductsFromBasket,
  resetDb,
  createLegalUser,
  createWallet,
  createCardRegistration,
  saveCardRegistration,
  payInCardDirect,
  getUserCards,
  updateOrderAddress,
  getUserOrder,
  addUserAddress,
  historyAddOrder,
  getUserOrderHistory,
  getUserOrderHistoryById
}))
