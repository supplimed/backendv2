import { User } from '../models/User'

export async function getCurrentUser ({
  userId,
  userRepository
}): Promise<User> {
  return userRepository.getUserByIdFilesystem(userId)
}
