import { AddressDoesNotExist, NotFoundError } from '../errors'

export async function updateOrderAddress ({
  id,
  deliveryAddress,
  billingAddress,
  orderRepository,
  userRepository
}): Promise<void> {
  let dbUser

  try {
    dbUser = await userRepository.getUserByIdFilesystem(id)
  } catch (error) {
    throw new NotFoundError()
  }

  const deliveryAddressCheck = dbUser.addresses.find(elem => elem.address === deliveryAddress.address &&
                                                           elem.postalCode === deliveryAddress.postalCode &&
                                                           elem.city === deliveryAddress.city)

  const billingAddressCheck = dbUser.addresses.find(elem => elem.address === billingAddress.address &&
                                                                  elem.postalCode === billingAddress.postalCode &&
                                                                  elem.city === billingAddress.city)

  if (!deliveryAddressCheck || !billingAddressCheck) {
    throw new AddressDoesNotExist()
  }

  return await orderRepository.updateOrderAddressFilesystem(id, deliveryAddress, billingAddress)
}
