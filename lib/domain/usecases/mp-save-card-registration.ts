import { MangoPayMissingClientIdOnUserError, NotFoundError, MangoPayFilesystemCardIdError, MangoPayMismatchMpClientIdError, PayloadIsInvalidError } from '../errors'
import { User } from '../models/User'

export async function saveCardRegistration ({
  userId,
  mpUserId,
  mpCardId,
  userRepository
}): Promise<boolean> {
  // Payload checking
  if (typeof mpUserId !== 'string' || typeof mpCardId !== 'string') {
    throw new PayloadIsInvalidError()
  }

  let userDomain: User
  try {
    userDomain = await userRepository.getUserByIdFilesystem(userId)
  } catch (error) {
    throw new NotFoundError()
  }

  if (userDomain.mpClientId === 'undefined' || !userDomain.mpClientId) {
    throw new MangoPayMissingClientIdOnUserError()
  }

  if (userDomain.mpClientId !== mpUserId) {
    throw new MangoPayMismatchMpClientIdError()
  }

  // Update in database

  try {
    userDomain.mpCardId.push(mpCardId)
    await userRepository.updateUser(userDomain)
  } catch (error) {
    throw (new MangoPayFilesystemCardIdError())
  }

  return true
}
