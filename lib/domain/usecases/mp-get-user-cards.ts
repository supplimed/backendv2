import { User } from '../models/User'
import mangoPay from '../../mangopay-config'
import { MangoPayMissingClientIdOnUserError, NotFoundError } from '../errors'
import { RegisteredCardI } from '../models/Payment'

export async function getUserCards ({
  id,
  userRepository
}) {
  let userDomain: User

  try {
    userDomain = await userRepository.getUserByIdFilesystem(id)
  } catch (error) {
    throw new NotFoundError()
  }

  if (!userDomain.mpClientId) {
    throw new MangoPayMissingClientIdOnUserError()
  }

  const cards = await mangoPay.Users.getCards(userDomain.mpClientId)

  if (!cards.length) {
    return null
  }

  const res: RegisteredCardI[] = []
  for (const card of cards) {
    res.push({
      expirationDate: card.ExpirationDate,
      alias: card.Alias,
      cardType: card.CardType,
      id: card.Id
    })
  }
  return res
}
