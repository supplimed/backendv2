import { MangoPayInvalidParametersError, PayloadIsInvalidError, MangoPayFilesystemClientIdError, MangoPayClientIdAlreadyExistsError } from '../errors'
import { User } from '../models/User'
import mangoPay from '../../mangopay-config'
import * as securityService from '../services/security-service'
import * as MangoPay from 'mangopay2-nodejs-sdk'

export async function createLegalUser ({
  mpUser,
  userId,
  userRepository
}): Promise<MangoPay.user.UserLegalData> {
  if (securityService.createLegalUserPayloadIsInvalid(mpUser)) {
    throw new PayloadIsInvalidError()
  }

  // Check if the user is already registered in MangoPay

  const userDomain: User = await userRepository.getUserByIdFilesystem(userId)
  if (userDomain.mpClientId != null && userDomain.mpClientId.length > 0) {
    throw (new MangoPayClientIdAlreadyExistsError())
  }

  let mangoPayUser: MangoPay.models.UserLegal

  try {
    mangoPayUser = new mangoPay.models.UserLegal({
      Name: mpUser.Name,
      LegalPersonType: mpUser.LegalPersonType,
      LegalRepresentativeBirthday: mpUser.LegalRepresentativeBirthday,
      LegalRepresentativeCountryOfResidence: mpUser.LegalRepresentativeCountryOfResidence,
      LegalRepresentativeNationality: mpUser.LegalRepresentativeNationality,
      LegalRepresentativeFirstName: mpUser.LegalRepresentativeFirstName,
      LegalRepresentativeLastName: mpUser.LegalRepresentativeLastName,
      Email: mpUser.Email
    })
  } catch (error) {
    throw (new MangoPayInvalidParametersError())
  }

  const mpRes: MangoPay.user.UserLegalData = await mangoPay.Users.create(mangoPayUser)

  // Update in database

  try {
    userDomain.mpClientId = mpRes.Id
    await userRepository.updateUser(userDomain)
  } catch (error) {
    throw (new MangoPayFilesystemClientIdError())
  }

  return mpRes
}
