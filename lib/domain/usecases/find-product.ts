import { Product } from '../models/Product'

export async function findProductById ({
  productId,
  productRepository
}): Promise<Product> {
  return await productRepository.findProductById(productId)
}
