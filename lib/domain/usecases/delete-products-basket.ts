export async function deleteProductsFromBasket ({
  id,
  orderRepository
}): Promise<boolean> {
  return await orderRepository.deleteProductsFromBasketFilesystem(id)
}
