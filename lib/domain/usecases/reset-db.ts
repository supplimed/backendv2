export async function resetDb ({
  testRepository
}): Promise<void> {
  return await testRepository.resetDb()
}
