import * as securityService from '../services/security-service'
import { User } from '../models/User'
import {
  ForbiddenActionError,
  PasswordInvalidError,
  PasswordsMismatchError
} from '../../domain/errors'

export async function updateUserPassword ({
  userId,
  updatedUserId,
  oldPassword,
  newPassword,
  newPasswordConfirmation,
  userRepository
}): Promise<User> {
  if (userId !== updatedUserId) {
    throw new ForbiddenActionError()
  }
  if (securityService.passwordsDoNotMatch(newPassword, newPasswordConfirmation)) {
    throw new PasswordsMismatchError()
  }
  if (securityService.isPasswordInvalid(newPassword)) {
    throw new PasswordInvalidError()
  }

  const userDomain = await userRepository.findUserByIdAndPasswordFilesystem(userId, oldPassword)

  return userRepository.updateUserWithPassword(userDomain, newPassword)
}
