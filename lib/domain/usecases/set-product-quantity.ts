import { BasketElement } from '../models/BasketElement'
import { Basket } from '../models/Basket'
import { DatabaseBasket } from '../../../db/filesystem/models/DatabaseBasket'
import { DatabaseBasketElement } from '../../../db/filesystem/models/DatabaseBasketElement'
import { Product } from '../models/Product'
import { NotFoundError, BasketInvalidError, PayloadIsInvalidError } from '../errors'
import * as securityService from '../services/security-service'

export async function setProductQuantity ({
  id,
  products,
  orderRepository,
  productRepository
}): Promise <Basket> {
  if (securityService.setProductQuantityPayloadIsInvalid(products)) {
    throw new PayloadIsInvalidError()
  }
  // Get the basket from the database
  let databaseBasket: DatabaseBasket
  try {
    databaseBasket = await orderRepository.findBasketByIdFilesystem(id)
  } catch (err) {
    if (err instanceof NotFoundError) { throw new NotFoundError() }
    throw new BasketInvalidError()
  }

  // Check if products exist in database & update the product quantity inside the basket
  for (const p of products) {
    await productRepository.findProductById(p.id)
    const updatedProduct: DatabaseBasketElement = databaseBasket.databaseBasketElement.find(elem => elem.productId === p.id)
    if (updatedProduct !== undefined && updatedProduct) { updatedProduct.quantity = p.quantity }
  }

  // Remove products which quantity equals 0
  databaseBasket.databaseBasketElement = databaseBasket.databaseBasketElement.filter(elem => elem.quantity > 0)

  /*
  * IF: delete the basket in database
  * ELSE: update the basket in database
  */
  if (databaseBasket.databaseBasketElement.length < 1) {
    await orderRepository.deleteProductsFromBasketFilesystem(databaseBasket.id)
  } else {
    databaseBasket = await orderRepository.updateBasketFilesystem(databaseBasket)
  }

  // Create BasketElements (domain) from the DatabaseBasketElement
  const basketElements: BasketElement[] = []
  for (const element of databaseBasket.databaseBasketElement) {
    const product: Product = await productRepository.findProductById(element.productId)
    basketElements.push(
      new BasketElement(
        product,
        element.quantity
      ))
  }

  return new Basket(
    databaseBasket.id,
    basketElements
  )
}
