import { User } from '../models/User'
import mangoPay from '../../mangopay-config'
import * as MangoPay from 'mangopay2-nodejs-sdk'
import {
  MangoPayMissingCardIdOnUserError,
  MangoPayMissingClientIdOnUserError,
  MangoPayMissingWalletIdOnUserError,
  NotFoundError
} from '../errors'

export async function payInCardDirect ({
  id,
  amount,
  cardId,
  userRepository
}): Promise<MangoPay.payIn.CardDirectPayInData> {
  let userDomain: User

  try {
    userDomain = await userRepository.getUserByIdFilesystem(id)
  } catch (error) {
    throw new NotFoundError()
  }

  if (!userDomain.mpClientId) {
    throw new MangoPayMissingClientIdOnUserError()
  }

  if (!userDomain.mpWalletId) {
    throw new MangoPayMissingWalletIdOnUserError()
  }

  if (!userDomain.mpCardId) {
    throw new MangoPayMissingCardIdOnUserError()
  }

  const debitedFunds: MangoPay.MoneyData = {
    Amount: amount,
    Currency: 'EUR'
  }

  const fees: MangoPay.MoneyData = {
    Amount: 0,
    Currency: 'EUR'
  }

  const payIn: MangoPay.payIn.CreateCardDirectPayIn = {
    ExecutionType: 'DIRECT',
    PaymentType: 'CARD',
    AuthorId: userDomain.mpClientId,
    CreditedWalletId: userDomain.mpWalletId,
    CardId: cardId,
    DebitedFunds: debitedFunds,
    Fees: fees,
    SecureModeReturnURL: 'http://localhost:4300/'
  }

  return await mangoPay.PayIns.create(payIn)
}
