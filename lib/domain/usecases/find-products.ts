import { Product } from '../models/Product'

const PAGINATION = {
  DEFAULT_PAGE: 1,
  DEFAULT_PAGE_SIZE: 20,
  MIN_PAGE: 1,
  MAX_PAGE_SIZE: 50
}

export interface PaginationInterface {
  page: number
  pageSize: number
  pageCount: number
  totalCount: number
}

export async function findProducts ({
  page,
  pageSize,
  productRepository
}): Promise<{ paginatedProducts: Product[], pagination: PaginationInterface}> {
  if (page < PAGINATION.MIN_PAGE) {
    page = PAGINATION.DEFAULT_PAGE
  }
  if (pageSize > PAGINATION.MAX_PAGE_SIZE) {
    pageSize = PAGINATION.MAX_PAGE_SIZE
  }
  pageSize = pageSize || PAGINATION.DEFAULT_PAGE_SIZE

  const totalCount: number = await productRepository.count()

  if (totalCount === 0) {
    return {
      paginatedProducts: [],
      pagination: null
    }
  }

  let pageCount: number = Math.trunc(totalCount / pageSize)
  if (totalCount % pageSize > 0) {
    pageCount += 1
  }
  page = page || PAGINATION.DEFAULT_PAGE
  if (page > pageCount) {
    page = pageCount
  }

  const paginatedProducts: Product[] = await productRepository.findProducts(page, pageSize)

  return {
    paginatedProducts,
    pagination: { page, pageSize, pageCount, totalCount }
  }
}
