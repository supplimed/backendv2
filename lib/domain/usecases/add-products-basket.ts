import { Basket } from '../models/Basket'
import { BasketElement } from '../models/BasketElement'
import { DatabaseBasket } from '../../../db/filesystem/models/DatabaseBasket'
import { DatabaseBasketElement } from '../../../db/filesystem/models/DatabaseBasketElement'
import { Product } from '../models/Product'
import { DatabaseOrder } from '../../../db/filesystem/models/DatabaseOrder'
import { Order } from '../models/Order'
import * as securityService from '../services/security-service'
import business from '../../business-rules'
import * as _ from 'lodash'

import {
  BasketInvalidError,
  NotFoundError,
  ProductInvalidQuantityError,
  PayloadIsInvalidError,
  OrderInvalidError
} from '../errors'

// Compare basketElement by productId (ASC)
function compare (a: DatabaseBasketElement, b: DatabaseBasketElement): number {
  if (a.productId < b.productId) {
    return -1
  }
  if (a.productId > b.productId) {
    return 1
  }
  return 0
}

// Sort and remove duplicate from basket elements
function normalizeDatabaseBasketElement (databaseBasketElement: DatabaseBasketElement[]): DatabaseBasketElement[] {
  // Ascending sort
  databaseBasketElement.sort(compare)

  // Sum duplicates products
  return databaseBasketElement.reduce((accumulator, cur) => {
    const id = cur.productId
    const found = accumulator.find(elem => elem.productId === id)
    if (found) {
      found.quantity += cur.quantity
      if (found.quantity > business.MAX_QUANTITY_BY_PRODUCT) { throw new ProductInvalidQuantityError() }
    } else accumulator.push(cur)
    return accumulator
  }, [])
}

export async function addProductsToBasket ({
  id,
  products,
  orderRepository,
  productRepository
}): Promise <Basket> {
  if (securityService.addProductsToBasketPayloadIsInvalid(products)) {
    throw new PayloadIsInvalidError()
  }

  // Check if products exist in database
  for (const p of products) {
    await productRepository.findProductById(p.id)
  }

  // Create or get the order
  let databaseOrder: DatabaseOrder
  try {
    databaseOrder = await orderRepository.findOrderByIdFilesystem(id)
  } catch (err) {
    if (!(err instanceof NotFoundError)) {
      throw new OrderInvalidError()
    }
  }
  if (!_.isObject(databaseOrder)) {
    databaseOrder = await orderRepository.createOrderFilesystem(new Order(id, null, null, null))
  }

  // Get the basket from the database
  let databaseBasket: DatabaseBasket
  try {
    databaseBasket = await orderRepository.findBasketByIdFilesystem(id)
  } catch (err) {
    if (!(err instanceof NotFoundError)) {
      throw new BasketInvalidError()
    }
  }

  // Create DatabaseBasketElements from incoming products
  const newDatabaseBasketElement: DatabaseBasketElement[] = []
  for (const element of products) {
    newDatabaseBasketElement.push(
      new DatabaseBasketElement(
        element.id,
        element.quantity
      ))
  }

  /*
  * IF: no basket found in the database, we create a new one for the user
  * ELSE: we use the basket of the user
  */
  if (!_.isObject(databaseBasket)) {
    databaseBasket = new DatabaseBasket(id, normalizeDatabaseBasketElement(newDatabaseBasketElement))
    databaseBasket = await orderRepository.createBasketFilesystem(databaseBasket)
  } else {
    for (const basketElementIterator of databaseBasket.databaseBasketElement) {
      newDatabaseBasketElement.push(DatabaseBasketElement.fromDatabaseBasketElement(basketElementIterator))
    }
    databaseBasket.databaseBasketElement = normalizeDatabaseBasketElement(newDatabaseBasketElement)
    databaseBasket = await orderRepository.updateBasketFilesystem(databaseBasket)
  }

  // Create BasketElements (domain) from the DatabaseBasketElement
  const basketElements: BasketElement[] = []
  for (const element of databaseBasket.databaseBasketElement) {
    const product: Product = await productRepository.findProductById(element.productId)
    basketElements.push(
      new BasketElement(
        product,
        element.quantity
      ))
  }

  return new Basket(
    databaseBasket.id,
    basketElements
  )
}
