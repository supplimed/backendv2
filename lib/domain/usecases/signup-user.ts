import * as securityService from '../services/security-service'
import { User } from '../models/User'
import {
  EmailAlreadyTakenError,
  EmailMalformedError,
  PasswordsMismatchError,
  PasswordInvalidError
} from '../../domain/errors'

export async function signupUser ({
  firstName,
  lastName,
  email,
  password,
  passwordConfirmation,
  userRepository
}): Promise<User> {
  if (await userRepository.isExistingByEmailFilesystem(email)) {
    throw new EmailAlreadyTakenError()
  }
  if (securityService.isEmailMalformed(email)) {
    throw new EmailMalformedError()
  }
  if (securityService.passwordsDoNotMatch(password, passwordConfirmation)) {
    throw new PasswordsMismatchError()
  }
  if (securityService.isPasswordInvalid(password)) {
    throw new PasswordInvalidError()
  }

  return userRepository.createUserFilesystem(
    firstName,
    lastName,
    email,
    password
  )
}
