import { DatabaseOrder } from '../../../db/filesystem/models/DatabaseOrder'
import { Order } from '../models/Order'
import { NotFoundError, OrderInvalidError } from '../errors'

export async function getUserOrder ({
  id,
  orderRepository
}): Promise<Order> {
  let databaseOrder: DatabaseOrder

  try {
    databaseOrder = await orderRepository.findOrderByIdFilesystem(id)
  } catch (err) {
    if (err instanceof NotFoundError) {
      // If no order is found, create an empty order in database and return an empty order for the user
      const order = new Order(
        id,
        null,
        null,
        null
      )

      await orderRepository.createOrderFilesystem(order)

      return order
    }
    throw new OrderInvalidError()
  }

  return Order.fromDatabaseOrder(databaseOrder, null)
}
