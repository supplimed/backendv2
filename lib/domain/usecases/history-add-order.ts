import { DatabaseOrder } from '../../../db/filesystem/models/DatabaseOrder'
import { NotFoundError } from '../errors'
import { DatabaseBasket } from '../../../db/filesystem/models/DatabaseBasket'
import { BasketElement } from '../models/BasketElement'
import { Basket } from '../models/Basket'
import { Order } from '../models/Order'
import { OrderHistory } from '../models/OrderHistory'
import { CreateOrderId } from '../services/create-order-id-service'

export async function historyAddOrder ({
  id,
  mpTransactionId,
  orderRepository,
  productRepository
}): Promise<boolean> {
  // Get the database order
  let databaseOrder: DatabaseOrder

  try {
    databaseOrder = await orderRepository.findOrderByIdFilesystem(id)
  } catch (err) {
    throw new NotFoundError()
  }

  // Transform the database order in an order
  const basketElements = []
  let databaseBasket: DatabaseBasket

  try {
    databaseBasket = await orderRepository.findBasketByIdFilesystem(id)
  } catch (err) {
    throw new NotFoundError()
  }

  for (const element of databaseBasket.databaseBasketElement) {
    const product = await productRepository.findProductById(element.productId)
    basketElements.push(new BasketElement(
      product,
      element.quantity
    ))
  }

  const order: Order = Order.fromDatabaseOrder(databaseOrder, new Basket(databaseBasket.id, basketElements))

  // Create OrderHistory

  const date = Date.now()
  const orderGenerator: CreateOrderId = new CreateOrderId()
  const orderId: string = orderGenerator.generateOrderId(date)
  const orderHistory: OrderHistory = new OrderHistory(orderId, id, order, date, mpTransactionId)

  // We save the order in the database

  return await orderRepository.createOrderHistoryFilesystem(orderHistory)
}
