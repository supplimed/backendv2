import { BasketElement } from '../models/BasketElement'
import { Basket } from '../models/Basket'
import { DatabaseBasket } from '../../../db/filesystem/models/DatabaseBasket'
import {
  BasketInvalidError,
  NotFoundError
} from '../errors'

export async function getUserBasket ({
  id,
  orderRepository,
  productRepository
}): Promise <Basket> {
  const basketElements = []
  let databaseBasket: DatabaseBasket

  try {
    databaseBasket = await orderRepository.findBasketByIdFilesystem(id)
  } catch (err) {
    if (err instanceof NotFoundError) {
      // If no basket is found, return an empty basket for the user
      return new Basket(
        id,
        basketElements
      )
    }
    throw new BasketInvalidError()
  }

  for (const element of databaseBasket.databaseBasketElement) {
    const product = await productRepository.findProductById(element.productId)
    basketElements.push(new BasketElement(
      product,
      element.quantity
    ))
  }

  return new Basket(
    databaseBasket.id,
    basketElements
  )
}
