import { User } from '../models/User'

export function logUserIn ({
  email,
  password,
  userRepository
}): Promise<User> {
  return userRepository.findUserByEmailAndPasswordFilesystem(email, password)
}
