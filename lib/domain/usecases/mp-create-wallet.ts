import mangoPay from '../../mangopay-config'
import * as MangoPay from 'mangopay2-nodejs-sdk'
import { MangoPayInvalidParametersError, MangoPayFilesystemWalletIdError } from '../errors'
import { User } from '../models/User'

export async function createWallet ({
  mpUserId,
  userId,
  userRepository
}): Promise<MangoPay.wallet.WalletData> {
  // Create the wallet in MangoPay

  let wallet: MangoPay.models.Wallet
  try {
    wallet = new mangoPay.models.Wallet({
      Owners: [mpUserId],
      Description: `Wallet of user n°${mpUserId}`,
      Currency: 'EUR'
    })
  } catch (error) {
    throw (new MangoPayInvalidParametersError())
  }

  const mpRes: MangoPay.wallet.WalletData = await mangoPay.Wallets.create(wallet)

  // Update in database

  try {
    const userDomain: User = await userRepository.getUserByIdFilesystem(userId)
    userDomain.mpWalletId = mpRes.Id
    await userRepository.updateUser(userDomain)
  } catch (error) {
    throw (new MangoPayFilesystemWalletIdError())
  }

  return mpRes
}
