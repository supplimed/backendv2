import { NotFoundError, OrderInvalidError } from '../errors'
import { OrderHistory } from '../models/OrderHistory'
import { DatabaseHistoryOrderHistory } from '../../../db/filesystem/models/history/DatabaseHistoryOrderHistory'

export async function getUserOrderHistoryById ({
  id,
  orderHistoryId,
  orderRepository
}): Promise<OrderHistory> {
  let databaseOrderHistoryHistory: DatabaseHistoryOrderHistory

  try {
    databaseOrderHistoryHistory = await orderRepository.findOrderHistoryByIdFilesystem(id, orderHistoryId)
  } catch (err) {
    if (err instanceof NotFoundError) {
      throw new NotFoundError()
    }
    throw new OrderInvalidError()
  }

  return OrderHistory.fromDatabaseOrderHistoryHistory(databaseOrderHistoryHistory)
}
