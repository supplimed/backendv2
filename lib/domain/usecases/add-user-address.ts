import { DatabaseUser } from '../../../db/filesystem/models/DatabaseUser'
import { AddressAlreadySaved, NotFoundError } from '../errors'

export async function addUserAddress ({
  id,
  address,
  userRepository
}): Promise<DatabaseUser> {
  let dbUser

  try {
    dbUser = await userRepository.getUserByIdFilesystem(id)
  } catch (error) {
    throw new NotFoundError()
  }

  if (address) {
    const addressCheck = dbUser.addresses.find(elem => elem.address === address.address &&
        elem.postalCode === address.postalCode &&
        elem.city === address.city)

    if (!addressCheck) {
      dbUser.addresses.push(address)
    } else {
      throw new AddressAlreadySaved()
    }
  }

  return await userRepository.updateUser(dbUser)
}
