import mangoPay from '../../mangopay-config'
import * as MangoPay from 'mangopay2-nodejs-sdk'
import { MangoPayInvalidParametersError, MangoPayMissingClientIdOnUserError, MangoPayCreateCardRegistrationError, NotFoundError, MangoPayFilesystemCardIdError } from '../errors'
import { User } from '../models/User'

export async function createCardRegistration ({
  id,
  userRepository
}): Promise<MangoPay.cardRegistration.CardRegistrationData> {
  let userDomain: User
  try {
    userDomain = await userRepository.getUserByIdFilesystem(id)
  } catch (error) {
    throw new NotFoundError()
  }

  if (userDomain.mpClientId === 'undefined' || !userDomain.mpClientId) {
    throw new MangoPayMissingClientIdOnUserError()
  }

  let card: MangoPay.cardRegistration.CreateCardRegistration

  try {
    card = new mangoPay.models.CardRegistration({
      UserId: userDomain.mpClientId,
      Currency: 'EUR'
    })
  } catch (error) {
    throw (new MangoPayInvalidParametersError())
  }

  const mpRes: MangoPay.cardRegistration.CardRegistrationData = await mangoPay.CardRegistrations.create(card)

  if (typeof mpRes.PreregistrationData === 'undefined' || !mpRes.PreregistrationData ||
  typeof mpRes.Id === 'undefined' || !mpRes.Id ||
  typeof mpRes.AccessKey === 'undefined' || !mpRes.AccessKey ||
  typeof mpRes.CardRegistrationURL === 'undefined' || !mpRes.CardRegistrationURL) {
    throw new MangoPayCreateCardRegistrationError()
  }

  // Update in database

  try {
    await userRepository.updateUser(userDomain)
  } catch (error) {
    throw (new MangoPayFilesystemCardIdError())
  }

  return mpRes
}
