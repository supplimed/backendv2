import { DatabaseProduct } from '../../../db/filesystem/models/DatabaseProduct'
import { DatabaseHistoryProduct } from '../../../db/filesystem/models/history/DatabaseHistoryProduct'

export const enum CURRENCIES {
  EURO = 'euro',
  DOLLAR = 'dollar'
}

export class Product {
  public id: string
  public name: string
  public price: number
  public currency: CURRENCIES
  public description: string
  public imageUrl: string
  public type?: string

  static fromDatabaseProductHistory (dbProductHistory: DatabaseHistoryProduct): Product {
    return new Product(dbProductHistory.id, dbProductHistory.name, dbProductHistory.price, null, dbProductHistory.imageUrl, dbProductHistory.currency)
  }

  static fromDatabaseProduct (dbProduct: DatabaseProduct): Product {
    return new Product(dbProduct.id, dbProduct.name, dbProduct.price, dbProduct.description, dbProduct.imageUrl, dbProduct.currency)
  }

  static fromProduct (product: Product): Product {
    return new Product(product.id, product.name, product.price, product.description, product.imageUrl, product.currency)
  }

  constructor (id: string, name: string, price: number, description: string, imageUrl: string, currency: CURRENCIES = CURRENCIES.EURO) {
    this.id = id
    this.name = name
    this.price = price
    this.currency = currency || CURRENCIES.EURO
    this.description = description
    this.imageUrl = imageUrl
  }
}
