import { DatabaseAddress } from '../../../db/filesystem/models/DatabaseAddress'

export class Address {
    public address: string
    public postalCode: string
    public city: string

    static fromDatabaseAddress ({ address, city, postalCode }: DatabaseAddress): Address {
      return new Address(address, postalCode, city)
    }

    static fromAddress ({ address, city, postalCode }: Address): Address {
      return new Address(address, postalCode, city)
    }

    constructor (
      address: string,
      postalCode: string,
      city: string
    ) {
      this.address = address
      this.postalCode = postalCode
      this.city = city
    }
}
