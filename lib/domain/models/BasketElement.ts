import { Product } from './Product'
import { DatabaseHistoryBasketElement } from '../../../db/filesystem/models/history/DatabaseHistoryBasketElement'

export class BasketElement {
  public product: Product
  public quantity: number
  public bulkPrice: number

  static fromDatabaseBasketElementHistory (dbBasketElementHistory: DatabaseHistoryBasketElement): BasketElement {
    return new BasketElement(Product.fromDatabaseProductHistory(dbBasketElementHistory.product), dbBasketElementHistory.quantity)
  }

  constructor (product: Product, quantity: number) {
    this.product = product
    this.quantity = quantity
    this.bulkPrice = this.quantity * this.product.price
  }

  public setQuantity (quantity: number): void{
    this.quantity = quantity
    this.bulkPrice = this.quantity * this.product.price
  }
}
