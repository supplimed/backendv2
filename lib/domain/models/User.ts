import { EmailInvalidError, EmailsMismatchError } from '../errors'
import * as _ from 'lodash'
import { DatabaseUser } from '../../../db/filesystem/models/DatabaseUser'
import { Address } from './Address'

export class User {
  public id: string
  public email: string
  public firstName: string
  public lastName: string
  public mpClientId: string
  public mpWalletId: string
  public mpCardId: string[]
  public addresses: Address[]

  static fromDatabaseUser (dbUser: DatabaseUser): User {
    return new User(dbUser.id, dbUser.email, dbUser.firstName, dbUser.lastName, dbUser.mpClientId, dbUser.mpWalletId, dbUser.mpCardId, dbUser.addresses)
  }

  static fromUser (user: User): User {
    return new User(user.id, user.email, user.firstName, user.lastName, user.mpClientId, user.mpWalletId, user.mpCardId, user.addresses)
  }

  constructor (
    id: string,
    email: string,
    firstName: string,
    lastName: string,
    mpClientId?: string,
    mpWalletId?: string,
    mpCardId?: string[],
    addresses?: Address[]
  ) {
    this.id = id
    this.email = email
    this.firstName = firstName
    this.lastName = lastName
    this.mpClientId = mpClientId || null
    this.mpWalletId = mpWalletId || null
    mpCardId ? this.mpCardId = mpCardId.slice() : this.mpCardId = null
    addresses ? this.addresses = JSON.parse(JSON.stringify(addresses)) : this.addresses = null
  }

  updateUserEmail (newEmail: string, newEmailConfirmation: string, { securityService }): void {
    if (securityService.isEmailMalformed(newEmail)) {
      throw new EmailInvalidError({ invalidEmail: newEmail })
    }
    if (!_.isEqual(newEmail, newEmailConfirmation)) {
      throw new EmailsMismatchError()
    }
    this.email = newEmail
  }
}
