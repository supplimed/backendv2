export interface RegisteredCardI {
    expirationDate: string
    alias: string
    cardType: string
    id: string
}
