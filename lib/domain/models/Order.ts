import { Address } from './Address'
import { Basket } from './Basket'
import { DatabaseOrder } from '../../../db/filesystem/models/DatabaseOrder'
import { DatabaseHistoryOrder } from '../../../db/filesystem/models/history/DatabaseHistoryOrder'

export class Order {
    public id: string
    public basket: Basket
    public deliveryAddress: Address
    public billingAddress: Address

    static fromDatabaseOrder (databaseOrder: DatabaseOrder, basket: Basket): Order {
      return new Order(databaseOrder.id, basket, databaseOrder.deliveryAddress, databaseOrder.billingAddress)
    }

    /***
     * order.id and order.basket.id must be set to userId later
     */
    static fromDatabaseOrderHistory (databaseOrder: DatabaseHistoryOrder): Order {
      return new Order(null, Basket.fromDatabaseBasketHistory(databaseOrder.basket), databaseOrder.deliveryAddress, databaseOrder.billingAddress)
    }

    constructor (id: string, basket: Basket, deliveryAddress: Address, billingAddress: Address) {
      this.id = id
      this.basket = basket || null
      this.deliveryAddress = deliveryAddress || null
      this.billingAddress = billingAddress || null
    }
}
