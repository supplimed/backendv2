import { BasketElement } from './BasketElement'
import { DatabaseHistoryBasket } from '../../../db/filesystem/models/history/DatabaseHistoryBasket'
import * as _ from 'lodash'

export class Basket {
  public id: string
  public basketElements: BasketElement[]
  private totalPrice?: number
  private totalProductsQuantity: number

  static fromDatabaseBasketHistory (dbBasketHistory: DatabaseHistoryBasket): Basket {
    // Create BasketElement array from DatabaseBasketElementHistory array
    const basketElements: BasketElement[] = []
    dbBasketHistory.basketElements.forEach(element => basketElements.push(BasketElement.fromDatabaseBasketElementHistory(element)))

    return new Basket(null, basketElements)
  }

  constructor (id: string, basketElements: BasketElement[]) {
    this.id = id
    this.basketElements = basketElements
    // multiply by 100 to use mangoPay annotation (1000 = 10.00)
    this.totalPrice = _.sumBy(this.basketElements, 'bulkPrice') * 100
    this.totalProductsQuantity = _.sumBy(this.basketElements, 'quantity')
  }

  public updateTotalPriceAndQuantity (): void {
    this.totalPrice = _.sumBy(this.basketElements, 'bulkPrice') * 100
    this.totalProductsQuantity = _.sumBy(this.basketElements, 'quantity')
  }

  public getTotalPrice (): number {
    return this.totalPrice
  }
}
