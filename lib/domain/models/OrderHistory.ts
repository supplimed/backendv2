import { Order } from './Order'
import { DatabaseHistoryOrderHistory } from '../../../db/filesystem/models/history/DatabaseHistoryOrderHistory'

export class OrderHistory {
    public id: string
    public order: Order
    public date: number
    public mpTransactionId: string
    public userId: string

    static fromDatabaseOrderHistoryHistory (orderHistory: DatabaseHistoryOrderHistory): OrderHistory {
      return new OrderHistory(orderHistory.id, orderHistory.userId, Order.fromDatabaseOrderHistory(orderHistory.order), orderHistory.date, orderHistory.mpTransactionId)
    }

    constructor (id: string, userId: string, order: Order, date: number, mpTransactionId: string) {
      this.id = id
      this.userId = userId
      this.order = order
      this.date = date
      this.mpTransactionId = mpTransactionId

      if (this.order !== null && this.order.id === null) {
        this.order.id = this.userId
      }

      if (this.order !== null && this.order.basket !== null && this.order.basket.id === null) {
        this.order.basket.id = this.userId
      }
    }
}
