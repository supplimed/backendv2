import * as Joi from '@hapi/joi'
import * as _ from 'lodash'
import * as emailValidator from 'email-validator'
import * as jsonwebtoken from 'jsonwebtoken'
import config from '../../config'
import { User } from '../models/User'
import { DatabaseUser } from '../../../db/filesystem/models/DatabaseUser'
import business from '../../business-rules'

interface JwtParams {
  userId: string
  secret: string
  expiresIn: string
}

interface JwtVerifyResult {
  userId
  iat
  exp
}

const PASSWORD_SCHEMA = Joi.string().min(8).max(30)

function _createToken ({ userId, secret, expiresIn }: JwtParams): string {
  return jsonwebtoken.sign({ userId }, secret, { expiresIn })
}

/**
 * Calculate the age from a birthday date
 * @param birthday date to check
 */
function _calculateAge (birthday: Date): number {
  const ageDifMs = Date.now() - birthday.getTime()
  // miliseconds from epoch
  const ageDate = new Date(ageDifMs)
  return Math.abs(ageDate.getUTCFullYear() - 1970)
}

/**
 * Check if the user birthday matches our business policies
 * @param date birthday date to check
 */
function _dateIsInvalid (date: string): boolean {
  const elem = new Date(date)
  if (!elem || typeof elem.getMonth !== 'function') { return true }
  if (elem.getFullYear() < 1920 || _calculateAge(elem) < 18) { return true }
  return false
}

/**
 * Verify if payload matchs with business rules
 * @param products incoming products
 */
export function addProductsToBasketPayloadIsInvalid (products: {id: string, quantity: number}[]): boolean {
  if (products.length > business.MAX_PRODUCTS_BY_ORDER) {
    return true
  }

  for (const iterator of products) {
    if (iterator.id.length < 1 || iterator.id.length > business.MAX_LENGTH_FOR_ID ||
      iterator.quantity < 1 || iterator.quantity > business.MAX_QUANTITY_BY_PRODUCT) {
      return true
    }
  }

  return false
}

export function createLegalUserPayloadIsInvalid (mpUser: any): boolean {
  for (const elem of Object.values(mpUser)) {
    if (elem === undefined || elem === null) {
      return true
    }

    if (typeof elem === 'string' && elem.length <= business.MIN_LENGTH_FOR_STRING && elem.length >= business.MAX_LENGTH_FOR_STRING) {
      return true
    }
  }

  if (_dateIsInvalid(mpUser.LegalRepresentativeBirthday)) { return true }

  return false
}

export function createTokenFromUser (user: User | DatabaseUser): string {
  return _createToken({
    userId: user.id,
    secret: config.auth.tokenSecret,
    expiresIn: config.auth.tokenLifespan
  })
}

export function extractInfoFromToken (token: string): string | object {
  const result: any = jsonwebtoken.verify(token, config.auth.tokenSecret)
  return typeof result === 'string' ? result : result.userId
}

export function isEmailMalformed (email: string): boolean {
  return !emailValidator.validate(email)
}

export function isPasswordInvalid (password: string): boolean {
  return !isPasswordValid(password)
}

export function isPasswordValid (password: string): boolean {
  const validation = PASSWORD_SCHEMA.validate(password)
  return _.isEmpty(validation.error)
}

export function passwordsDoNotMatch (password: string, passwordConfirmation: string): boolean {
  return password !== passwordConfirmation
}

/**
 * Verify if payload matchs with business rules
 * @param products incoming products
 */
export function setProductQuantityPayloadIsInvalid (products: {id: string, quantity: number}[]): boolean {
  if (products.length !== 1) { return true }

  for (const iterator of products) {
    if (iterator.id.length < 1 || iterator.id.length > business.MAX_LENGTH_FOR_ID ||
      iterator.quantity < 0 || iterator.quantity > business.MAX_QUANTITY_BY_PRODUCT) {
      return true
    }
  }

  return false
}
