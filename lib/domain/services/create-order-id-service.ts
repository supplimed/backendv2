import * as crypto from 'crypto'
import config from '../../config'

/**
 * Class generating an order id
 * Based on "Generate a random order-number" from Michael DeRazon (www.drzon.net)
 *  https://github.com/mderazon/order-id
 */
export class CreateOrderId {
  private domain: string[] = config.idGenerator.domain
  private algorithm: string = config.idGenerator.algorithm
  private encTable: string[]
  private decTable: string[]

  constructor () {
    // Create a permutation of domain

    const sorted = this.domain
      .map(c => c)
      .sort((c1, c2) => this.enc(c1).localeCompare(this.enc(c2)))

    this.encTable = []
    this.decTable = []

    for (const i in this.domain) {
      this.encTable[this.domain[i]] = sorted[i]
      this.decTable[sorted[i]] = this.domain[i]
    }
  }

  public decrypt (text: string): string {
    if (typeof text !== 'string') {
      throw new Error('Input is not a string')
    }
    const decrypted: string = text
      .split('')
      .map(c => this.decTable[c])
      .join('')
    this.validate(text, decrypted)
    return decrypted
  }

  private enc (text: string): string {
    const cipher: crypto.Cipher = crypto.createCipheriv(this.algorithm, Buffer.from(crypto.randomBytes(32)), crypto.randomBytes(16))
    let crypted: string = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex')
    return crypted
  }

  private encrypt (text): string {
    if (typeof text !== 'string') {
      throw new Error('Input is not a string')
    }
    const encrypted: string = text
      .split('')
      .map(c => this.encTable[c])
      .join('')
    this.validate(text, encrypted)
    return encrypted
  }

  public generateOrderId (date): string {
    let now = date
      ? new Date(date).getTime().toString()
      : Date.now().toString()

    // pad with additional random digits
    if (now.length < 14) {
      const pad = 14 - now.length
      now += this.randomNumber(pad)
    }
    now = this.encrypt(now)

    // split into xxxx-xxxxxx-xxxx format
    return [now.slice(0, 4), now.slice(4, 10), now.slice(10, 14)].join('-')
  }

  private randomNumber (length: number): string {
    return Math.floor(
      Math.pow(10, length - 1) +
        Math.random() * (Math.pow(10, length) - Math.pow(10, length - 1) - 1)
    ).toString()
  }

  private validate (text: string, result: string): void{
    if (text.length !== result.length) {
      throw new Error(
        `Some of the input characters are not in the cipher's domain: [${this.domain}]`
      )
    }
  }
}
