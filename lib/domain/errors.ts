export class BasketInvalidError extends Error {
  errorCode = 400
  status = 1
  constructor (message = 'Basket invalid error') {
    super(message)
    Object.setPrototypeOf(this, BasketInvalidError.prototype)
  }
}

export class EmailAlreadyTakenError extends Error {
  constructor (message = 'Email already taken') {
    super(message)
    Object.setPrototypeOf(this, EmailAlreadyTakenError.prototype)
  }
}

export class EmailInvalidError extends Error {
  invalidEmail
  constructor ({ message = 'Email invalid error', invalidEmail }) {
    super(message)
    this.invalidEmail = invalidEmail
    Object.setPrototypeOf(this, EmailInvalidError.prototype)
  }
}

export class EmailMalformedError extends Error {
  constructor (message = 'Invalid password during sign up') {
    super(message)
    Object.setPrototypeOf(this, EmailMalformedError.prototype)
  }
}

export class EmailsMismatchError extends Error {
  constructor (message = 'Emails mismatch error') {
    super(message)
    Object.setPrototypeOf(this, EmailsMismatchError.prototype)
  }
}

export class ForbiddenActionError extends Error {
  constructor (message = 'Forbidden, profile update is only allowed for oneself') {
    super(message)
    Object.setPrototypeOf(this, ForbiddenActionError.prototype)
  }
}

export class NotFoundError extends Error {
  errorCode = 404
  status = 1
  constructor (message = 'Resource not found') {
    super(message)
    Object.setPrototypeOf(this, NotFoundError.prototype)
  }
}

export class OrderInvalidError extends Error {
  errorCode = 400
  status = 1
  constructor (message = 'Order invalid error') {
    super(message)
    Object.setPrototypeOf(this, OrderInvalidError.prototype)
  }
}

export class PasswordInvalidError extends Error {
  constructor (message = 'Password invalid error') {
    super(message)
    Object.setPrototypeOf(this, PasswordInvalidError.prototype)
  }
}

export class PasswordLoginDoesNotMatchError extends Error {
  constructor (message = 'Invalid password during login') {
    super(message)
    Object.setPrototypeOf(this, PasswordLoginDoesNotMatchError.prototype)
  }
}

export class PasswordsMismatchError extends Error {
  constructor (message = 'Password mismatch error') {
    super(message)
    Object.setPrototypeOf(this, PasswordsMismatchError.prototype)
  }
}

export class PasswordSignupDoesNotMatchError extends Error {
  constructor (message = 'Invalid password during sign up') {
    super(message)
    Object.setPrototypeOf(this, PasswordSignupDoesNotMatchError.prototype)
  }
}

export class PayloadIsInvalidError extends Error {
  errorCode = 400
  status = 1
  constructor (message = 'Unsafe payload') {
    super(message)
    Object.setPrototypeOf(this, PayloadIsInvalidError.prototype)
  }
}

export class ProductInvalidQuantityError extends Error {
  errorCode = 400
  constructor (message = 'Invalid quantity for a product') {
    super(message)
    Object.setPrototypeOf(this, ProductInvalidQuantityError.prototype)
  }
}

// MangoPay

export class MangoPayCreateCardRegistrationError extends Error {
  errorCode = 400
  status = 1
  constructor (message = 'Missing fields in data provided by MangoPay') {
    super(message)
    Object.setPrototypeOf(this, MangoPayCreateCardRegistrationError.prototype)
  }
}

export class MangoPayClientIdAlreadyExistsError extends Error {
  errorCode = 400
  status = 1
  constructor (message = 'User has already an MangoPay account') {
    super(message)
    Object.setPrototypeOf(this, MangoPayClientIdAlreadyExistsError.prototype)
  }
}

export class MangoPayFilesystemClientIdError extends Error {
  errorCode = 500
  status = 1
  constructor (message = 'Cannot update the client id user in the database') {
    super(message)
    Object.setPrototypeOf(this, MangoPayFilesystemClientIdError.prototype)
  }
}

export class MangoPayFilesystemWalletIdError extends Error {
  errorCode = 500
  status = 1
  constructor (message = 'Cannot update the wallet id user in the database') {
    super(message)
    Object.setPrototypeOf(this, MangoPayFilesystemWalletIdError.prototype)
  }
}

export class MangoPayFilesystemCardIdError extends Error {
  constructor (message = 'Cannot update the card id user in the database') {
    super(message)
    Object.setPrototypeOf(this, MangoPayFilesystemCardIdError.prototype)
  }
}

export class MangoPayInvalidParametersError extends Error {
  errorCode = 400
  status = 1
  constructor (message = 'One or several required parameters are missing or incorrect') {
    super(message)
    Object.setPrototypeOf(this, MangoPayInvalidParametersError.prototype)
  }
}

export class MangoPayMismatchMpClientIdError extends Error {
  constructor (message = 'The incoming mpUserId doesn\'t match the user mpUserId') {
    super(message)
    Object.setPrototypeOf(this, MangoPayMismatchMpClientIdError.prototype)
  }
}

export class MangoPayMissingClientIdOnUserError extends Error {
  errorCode = 400
  status = 1
  constructor (message = 'The user doesn\'t have a valid MangoPay Client Id') {
    super(message)
    Object.setPrototypeOf(this, MangoPayMissingClientIdOnUserError.prototype)
  }
}

export class MangoPayMissingWalletIdOnUserError extends Error {
  errorCode = 400
  status = 1
  constructor (message = 'The user doesn\'t have a valid MangoPay Wallet Id') {
    super(message)
    Object.setPrototypeOf(this, MangoPayMissingWalletIdOnUserError.prototype)
  }
}

export class MangoPayMissingCardIdOnUserError extends Error {
  errorCode = 400
  status = 1
  constructor (message = 'The user doesn\'t have a valid MangoPay Card Id') {
    super(message)
    Object.setPrototypeOf(this, MangoPayMissingCardIdOnUserError.prototype)
  }
}

export class AddressDoesNotExist extends Error {
  errorCode = 400
  status = 1
  constructor (message = 'The user selected an address that does not exist') {
    super(message)
    Object.setPrototypeOf(this, AddressDoesNotExist.prototype)
  }
}

export class AddressIsEmpty extends Error {
  constructor (message = 'The address is empty') {
    super(message)
    Object.setPrototypeOf(this, AddressIsEmpty.prototype)
  }
}

export class AddressAlreadySaved extends Error {
  constructor (message = 'The address is already saved') {
    super(message)
    Object.setPrototypeOf(this, AddressAlreadySaved.prototype)
  }
}
