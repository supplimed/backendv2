import { Basket } from '../../domain/models/Basket'

export default {
  serialize (domainBasket: Basket): {domainBasket: Basket} {
    return {
      domainBasket
    }
  }
}
