import { User } from '../../domain/models/User'

interface UserSerializerInterface {
  data: User
  meta: {}
}

export default {
  serialize (domainUser: User): UserSerializerInterface {
    return {
      data: domainUser,
      meta: {}
    }
  }
}
