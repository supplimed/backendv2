import * as _ from 'lodash'
import { Product } from '../../domain/models/Product'
import { PaginationInterface } from '../../domain/usecases/find-products'

function _serializeProductData (domainProduct: Product): Product {
  const serialized = _.cloneDeep(domainProduct)
  serialized.type = 'product'
  return serialized
}

export default {
  serializeCollection (domainProductsPaginated: Product[], pagination: PaginationInterface): {data: Product[], pagination: PaginationInterface} {
    return {
      data: _.map(domainProductsPaginated, _serializeProductData),
      pagination
    }
  },

  serializeData (domainProduct: Product): {data: Product} {
    return {
      data: _serializeProductData(domainProduct)
    }
  }
}
