import { Product } from '../../domain/models/Product'
import { DatabaseProduct } from '../../../db/filesystem/models/DatabaseProduct'
import { NotFoundError } from '../../domain/errors'
import { fsd as filesystemDatabase } from '../../../db/filesystem'
import * as _ from 'lodash'

function _toDomain (databaseProduct: DatabaseProduct): Product {
  return Product.fromDatabaseProduct(databaseProduct)
}

function _fetchPage (databaseProducts: DatabaseProduct[], page: number, pageSize: number): DatabaseProduct[] {
  return _.chunk(databaseProducts, pageSize)[page - 1]
}

export default {
  async findProducts (page: number, pageSize: number): Promise<Product[]> {
    const rawDatabaseProducts: DatabaseProduct[] = await filesystemDatabase.getCollectionByName('products')
    const databaseProducts: DatabaseProduct[] = _fetchPage(rawDatabaseProducts, page, pageSize)
    return _.map(databaseProducts, _toDomain)
  },

  async findProductById (id: string): Promise<Product> {
    const product = await filesystemDatabase.getItemById('products', id)
    if (product === undefined || !product) { throw new NotFoundError() }
    return _toDomain(product)
  },

  async count (): Promise<number> {
    return filesystemDatabase.getCollectionByName('products')
      .then((databaseProducts) => _.size(databaseProducts))
  }
}
