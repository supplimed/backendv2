import * as _ from 'lodash'
import { DatabaseBasket } from '../../../db/filesystem/models/DatabaseBasket'
import { fsd as filesystemDatabase } from '../../../db/filesystem'
import { NotFoundError } from '../../domain/errors'
import { DatabaseOrder } from '../../../db/filesystem/models/DatabaseOrder'
import { Order } from '../../domain/models/Order'
import { DatabaseAddress } from '../../../db/filesystem/models/DatabaseAddress'
import { OrderHistory } from '../../domain/models/OrderHistory'
import { DatabaseHistoryOrderHistory } from '../../../db/filesystem/models/history/DatabaseHistoryOrderHistory'

async function _findBasketByIdFilesystem (id): Promise<DatabaseBasket> {
  const baskets = await filesystemDatabase.getCollectionByName('baskets')
  return _.find(baskets, { id })
}

async function _findOrderByIdFilesystem (id): Promise<DatabaseOrder> {
  const orders = await filesystemDatabase.getCollectionByName('orders')
  return _.find(orders, { id })
}

async function _findOrderHistoryByIdFilesystem (userId: string, orderHistoryId: string): Promise<DatabaseHistoryOrderHistory> {
  const ordersHistory: DatabaseHistoryOrderHistory[] = await filesystemDatabase.getCollectionByName('ordersHistory')
  return ordersHistory.find(orderHistory => orderHistory.id === orderHistoryId && orderHistory.userId === userId)
}

async function _findOrderHistoryByUserIdFilesystem (id: string): Promise<DatabaseHistoryOrderHistory[]> {
  const ordersHistory: DatabaseHistoryOrderHistory[] = await filesystemDatabase.getCollectionByName('ordersHistory')
  return ordersHistory.filter(orderHistory => orderHistory.userId === id)
}

function _toDomainBasket (databaseBasket: DatabaseBasket): DatabaseBasket {
  return DatabaseBasket.fromDatabaseBasket(databaseBasket)
}

export default {
  async createBasketFilesystem (databaseBasket: DatabaseBasket): Promise<DatabaseBasket> {
    await filesystemDatabase.addBasket(databaseBasket)
    return await this.findBasketByIdFilesystem(databaseBasket.id)
  },

  async createOrderFilesystem (order: Order): Promise<DatabaseOrder> {
    await filesystemDatabase.addOrder(DatabaseOrder.fromOrder(order))
    return await this.findOrderByIdFilesystem(order.id)
  },

  async createOrderHistoryFilesystem (orderHistory: OrderHistory): Promise<boolean> {
    await filesystemDatabase.addOrderHistory(DatabaseHistoryOrderHistory.fromOrderHistory(orderHistory))
    return true
  },

  async deleteProductsFromBasketFilesystem (id: string): Promise<boolean> {
    await filesystemDatabase.deleteBasket(id)
    return true
  },

  async findBasketByIdFilesystem (id: string): Promise<DatabaseBasket> {
    const databaseBasket: DatabaseBasket = await _findBasketByIdFilesystem(id)
    if (databaseBasket === undefined || !databaseBasket) {
      throw new NotFoundError()
    }
    return _toDomainBasket(databaseBasket)
  },

  async findOrderByIdFilesystem (id: string): Promise<DatabaseOrder> {
    const databaseOrder: DatabaseOrder = await _findOrderByIdFilesystem(id)
    if (databaseOrder === undefined || !databaseOrder) {
      throw new NotFoundError()
    }
    return databaseOrder
  },

  async findOrderHistoryByUserIdFilesystem (id: string): Promise<DatabaseHistoryOrderHistory[]> {
    const databaseOrdersHistory: DatabaseHistoryOrderHistory[] = await _findOrderHistoryByUserIdFilesystem(id)
    if (databaseOrdersHistory === undefined || !databaseOrdersHistory) {
      throw new NotFoundError()
    }
    return databaseOrdersHistory
  },

  async findOrderHistoryByIdFilesystem (userId: string, orderHistoryId: string): Promise<DatabaseHistoryOrderHistory> {
    const databaseOrdersHistory: DatabaseHistoryOrderHistory = await _findOrderHistoryByIdFilesystem(userId, orderHistoryId)
    if (databaseOrdersHistory === undefined || !databaseOrdersHistory) {
      throw new NotFoundError()
    }
    return databaseOrdersHistory
  },

  async updateBasketFilesystem (databaseBasket: DatabaseBasket): Promise<DatabaseBasket> {
    await filesystemDatabase.updateBasket(databaseBasket)
    return await this.findBasketByIdFilesystem(databaseBasket.id)
  },

  async updateOrderAddressFilesystem (id: string, deliveryAddress: DatabaseAddress, billingAddress: DatabaseAddress): Promise<DatabaseOrder> {
    const databaseOrder: DatabaseOrder = await this.findOrderByIdFilesystem(id)
    databaseOrder.deliveryAddress = deliveryAddress
    databaseOrder.billingAddress = billingAddress
    await filesystemDatabase.updateOrder(databaseOrder)
    return await this.findOrderByIdFilesystem(id)
  }
}
