import { NotFoundError, PasswordLoginDoesNotMatchError } from '../../domain/errors'
import * as _ from 'lodash'
import { User } from '../../domain/models/User'
import * as bcrypt from 'bcrypt'
import { v4 as uuid } from 'uuid'
import { DatabaseUser } from '../../../db/filesystem/models/DatabaseUser'
import config from '../../config'
import { fsd as filesystemDatabase } from '../../../db/filesystem'

function _findUsersByEmail (users: DatabaseUser[], email: string): { emptyResult: boolean, multipleResults: boolean, user: DatabaseUser } {
  const usersMatchingEmail = _.filter(users, { email })
  return {
    emptyResult: _.isEmpty(usersMatchingEmail),
    multipleResults: _.size(usersMatchingEmail) > 1,
    user: _.first(usersMatchingEmail)
  }
}

async function _findUsersByEmailFilesystem (email: string): Promise<{ emptyResult: boolean, multipleResults: boolean, user: DatabaseUser }> {
  const users: DatabaseUser[] = await filesystemDatabase.getCollectionByName('users')
  return _findUsersByEmail(users, email)
}

async function _findUsersByIdFilesystem (id: string): Promise<DatabaseUser> {
  const users = await filesystemDatabase.getCollectionByName('users')
  return _.find(users, { id })
}

function _toDomain (databaseUser: DatabaseUser): User {
  return User.fromDatabaseUser(databaseUser)
}

export default {
  async getUserByIdFilesystem (id: string): Promise<User> {
    const user = await _findUsersByIdFilesystem(id)
    if (!user) throw new NotFoundError()
    return _toDomain(user)
  },

  async isExistingByEmailFilesystem (email: string): Promise<DatabaseUser> {
    const users = await filesystemDatabase.getCollectionByName('users')
    return _.find(users, { email })
  },

  async findUserByEmailAndPasswordFilesystem (email: string, password: string): Promise<User> {
    const { emptyResult, multipleResults, user } = await _findUsersByEmailFilesystem(email)

    if (emptyResult) {
      throw new NotFoundError()
    }
    if (multipleResults) {
      throw new Error('multiple matches')
    }
    const passwordIsNotValid = !bcrypt.compareSync(password, user.password)

    if (passwordIsNotValid) {
      throw new PasswordLoginDoesNotMatchError()
    }

    return _toDomain(user)
  },

  async findUserByIdAndPasswordFilesystem (userId: string, password: string): Promise<User> {
    const user = await _findUsersByIdFilesystem(userId)
    if (!user) {
      throw new NotFoundError()
    }

    const passwordIsNotValid = !bcrypt.compareSync(password, user.password)
    if (passwordIsNotValid) {
      throw new PasswordLoginDoesNotMatchError()
    }

    return _toDomain(user)
  },

  async createUserFilesystem (firstName: string, lastName: string, email: string, password: string): Promise<User> {
    const id = uuid()
    const encryptedPassword = await bcrypt.hashSync(password, config.auth.passwordSalt)
    const newUser: DatabaseUser = new DatabaseUser(id, email, encryptedPassword, firstName, lastName)
    await filesystemDatabase.addUser(newUser)
    return _toDomain(newUser)
  },

  async updateUser (user: User): Promise<User> {
    const updatedUser = await filesystemDatabase.updateUser(user)
    return _toDomain(updatedUser)
  },

  async updateUserWithPassword (user: User, password: string): Promise<User> {
    const updatedUser = await filesystemDatabase.updateUserWithPassword(user.id, password)
    return _toDomain(updatedUser)
  }
}
