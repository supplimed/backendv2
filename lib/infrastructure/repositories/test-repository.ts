import { fsd as filesystemDatabase } from '../../../db/filesystem'

export default {
  async resetDb (): Promise<void> {
    const isEmpty: boolean = await filesystemDatabase.checkDatabaseIsEmpty()

    if (!isEmpty) {
      await filesystemDatabase.emptyDatabase()
    }
    return await filesystemDatabase.seedDatabase()
  }
}
