import usecases from '../../../domain/usecases'

export default {
  async resetDb (request, h): Promise<void> {
    return usecases.resetDb({})
      .then(() => h.response().code(200))
  }
}
