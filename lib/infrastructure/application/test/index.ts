import testController from './test-controller'

export const testPlugin = {
  name: 'test-api',
  version: '1.0.0',
  register: async function (server): Promise<void> {
    server.route([
      {
        method: 'GET',
        path: '/test/reset',
        config: {
          handler: testController.resetDb,
          tags: ['api', 'test'],
          notes: [
            '- **Cette route permet de mettre la base de donnée dans un état initialisé**\n'
          ]
        }
      }
    ])
  }
}
