import healthcheckController from './healthcheck-controller'

export const healthcheckPlugin = {
  name: 'healthcheck-api',
  version: '1.0.0',
  register: async function (server): Promise<void> {
    server.route([
      {
        method: 'GET',
        path: '/status',
        config: {
          handler: healthcheckController.checkApiStatus,
          tags: ['api', 'healthy'],
          notes: [
            '- **Cette route permet de s\'assurer que l\'api est up and running**\n' +
            '- Renvoie le statut de l\'api'
          ]
        }
      }
    ])
  }
}
