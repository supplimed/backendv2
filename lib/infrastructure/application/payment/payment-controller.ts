import usecases from '../../../domain/usecases'
import * as securityService from '../../../domain/services/security-service'
import { Basket } from '../../../domain/models/Basket'
import { OrderHistory } from '../../../domain/models/OrderHistory'
import MangoPay = require('mangopay2-nodejs-sdk')

export default {
  /**
   * Create a Card Registration in mango pay
   * @param request contains payload
   * @param h response
   */
  async createCardRegistration (request, h): Promise<string> {
    let id
    try {
      const token = request.headers.authorization.split(/Bearer /i)[1]
      id = await securityService.extractInfoFromToken(token)
    } catch (err) {
      return h.response({ msg: 'Invalid token' }).code(401)
    }
    let cardRegistration: MangoPay.cardRegistration.CardRegistrationData

    try {
      cardRegistration = await usecases.createCardRegistration({ id })
    } catch (err) {
      return h.response({ errorDescription: err.message || 'usecase createCardRegistration failed', status: 1 || 2 }).code(err.errorCode || 500)
    }

    return h.response({
      cardRegistrationData: {
        Id: cardRegistration.Id,
        PreregistrationData: cardRegistration.PreregistrationData,
        AccessKey: cardRegistration.AccessKey,
        CardRegistrationURL: cardRegistration.CardRegistrationURL
      },
      status: 0
    })
  },

  /**
   * Create a Legal User and it wallet in MangoPay
   * @param request contains payload
   * @param h response
   */
  async createLegalUser (request, h): Promise<string> {
    let id
    try {
      const token = request.headers.authorization.split(/Bearer /i)[1]
      id = await securityService.extractInfoFromToken(token)
    } catch (err) {
      return h.response({ msg: 'Invalid token' }).code(401)
    }

    // Get mangopay user attributes
    const { user } = request.payload
    let mangoPayUser: MangoPay.user.UserLegalData
    let mangoPayWallet: MangoPay.wallet.WalletData

    // Create Legal User in MangoPay
    try {
      mangoPayUser = await usecases.createLegalUser({ mpUser: user, userId: id })
    } catch (err) {
      return h.response({ errorDescription: err.message || 'usecases createLegalUser failed', errors: err.errors, status: 1 || 2 }).code(err.errorCode || 500)
    }

    // Create Legal User's wallet
    try {
      mangoPayWallet = await usecases.createWallet({ mpUserId: mangoPayUser.Id, userId: id })
    } catch (err) {
      return h.response({ errorDescription: err.message || 'usecases createWallet failed', status: 1 || 2 }).code(err.errorCode || 500)
    }

    return h.response({ user: mangoPayUser, wallet: mangoPayWallet, status: 0 })
  },

  /**
   * Save Card Id in User
   * @param request contains payload
   * @param h response
   */
  async saveCardId (request, h): Promise<string> {
    let id
    try {
      const token = request.headers.authorization.split(/Bearer /i)[1]
      id = await securityService.extractInfoFromToken(token)
    } catch (err) {
      return h.response({ msg: 'Invalid token' }).code(401)
    }
    const { CardId, UserId } = request.payload

    let success: boolean
    try {
      success = await usecases.saveCardRegistration({ userId: id, mpUserId: UserId, mpCardId: CardId })
    } catch (err) {
      return h.response({ errorDescription: err.message || 'usecases saveCardRegistration failed', status: 1 || 2 }).code(err.errorCode || 500)
    }

    if (!success) {
      return h.response({ errorDescription: 'Save failed without error', status: 1 }).code(500)
    }
    return h.response({ status: 0 })
  },

  /**
   * Create direct card payIn in MangoPay
   * @param request contains payload
   * @param h response
   */
  async payInCardDirect (request, h): Promise<string> {
    let id
    try {
      const token = request.headers.authorization.split(/Bearer /i)[1]
      id = await securityService.extractInfoFromToken(token)
    } catch (err) {
      return h.response({ msg: 'Invalid token' }).code(401)
    }
    let mangoPayIn: MangoPay.models.PayIn
    const { cardId } = request.payload

    let basket: Basket
    try {
      basket = await usecases.getUserBasket({ id })
    } catch (err) {
      return h.response({ errorDescription: err.message, status: 1 }).code(err.errorCode)
    }

    try {
      mangoPayIn = await usecases.payInCardDirect({ id: id, amount: basket.getTotalPrice(), cardId })
    } catch (err) {
      return h.response({ errorDescription: err.message || 'usecases payInCardDirect failed', status: 1 || 2 }).code(err.errorCode || 500)
    }

    // Check if the payment was succesful
    if (mangoPayIn.Status !== 'SUCCEEDED') { return h.response({ errorDescription: 'Payment status is not SUCCEEDED', status: 1 }).code(400) }

    // Archive the order
    let historyIsSuccess: boolean
    try {
      historyIsSuccess = await usecases.historyAddOrder({ id: id, mpTransactionId: mangoPayIn.Id })
    } catch (err) {
      return h.response({ errorDescription: 'history AddOrder failed', status: 1 }).code(500)
    }

    if (!historyIsSuccess) { return h.response({ mpStatus: mangoPayIn.Status, errorDescription: 'history AddOrder failed', status: 1 }).code(400) }

    // Get the order
    let orderHistory: OrderHistory[]
    try {
      orderHistory = await usecases.getUserOrderHistory({ id })
    } catch (err) {
      return h.response({ errorDescription: err.message || 'usecases getUserOrderHistory failed', status: 1 || 2 }).code(err.errorCode || 500)
    }

    return h.response({ mpStatus: mangoPayIn.Status, orderHistoryId: orderHistory[0].id, resultCode: mangoPayIn.ResultCode, status: 0 })
  },

  /**
   * Get all user's credit cards MangoPay
   * @param request contains payload
   * @param h response
   */
  async getCreditCards (request, h): Promise<string> {
    let id
    try {
      const token = request.headers.authorization.split(/Bearer /i)[1]
      id = await securityService.extractInfoFromToken(token)
    } catch (err) {
      return h.response({ msg: 'Invalid token' }).code(401)
    }
    let cards: MangoPay.models.Card[]

    try {
      cards = await usecases.getUserCards({ id: id })
    } catch (err) {
      return h.response({ errorDescription: err.message || 'usecases getUserCards failed', status: err.status || 2 }).code(err.errorCode || 500)
    }

    return h.response({ cards: cards })
  }
}
