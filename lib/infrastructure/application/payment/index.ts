import paymentController from './payment-controller'

export const paymentPlugin = {
  name: 'payment-api',
  version: '1.0.0',
  register: async function (server): Promise<void> {
    server.route([
      {
        method: 'POST',
        path: '/mangopay/createLegalUser',
        config: {
          handler: paymentController.createLegalUser,
          tags: ['api', 'mangopay'],
          notes: [
            '- **Cette route crée un legal user ainsi que son wallet dans MangoPay.\n' +
            '- En cas de succès, renvoie le legal user, son wallet et le status code.'
          ]
        }
      },
      {
        method: 'GET',
        path: '/mangopay/cardregistration/create',
        config: {
          handler: paymentController.createCardRegistration,
          tags: ['api', 'mangopay'],
          notes: [
            '- **Cette route enregistre une carte dans MangoPay.\n' +
            '- En cas de succès, renvoie le user, le wallet et le status code.'
          ]
        }
      },
      {
        method: 'POST',
        path: '/mangopay/cardregistration/save',
        config: {
          handler: paymentController.saveCardId,
          tags: ['api', 'mangopay'],
          notes: [
            '- **Cette route enregistre les données reçues de la création d\'une carte dans MangoPay.\n' +
            '- En cas de succès, renvoie un status code.'
          ]
        }
      },
      {
        method: 'POST',
        path: '/mangopay/payin/card/direct',
        config: {
          handler: paymentController.payInCardDirect,
          tags: ['api', 'mangopay'],
          notes: [
            '- **Cette route permet de payer avec une carte déjà enregistrée.\n'
          ]
        }
      },
      {
        method: 'GET',
        path: '/mangopay/user/cards',
        config: {
          handler: paymentController.getCreditCards,
          tags: ['api', 'mangopay'],
          notes: [
            '- **Cette route permet de récupérer toutes les cartes d\'un utilisateur.\n'
          ]
        }
      }
    ])
  }
}
