import productsController from './products-controller'

export const productsPlugin = {
  name: 'products-api',
  version: '1.0.0',
  register: async function (server): Promise<void> {
    server.route([
      {
        method: 'GET',
        path: '/products',
        config: {
          handler: productsController.findProducts,
          tags: ['api', 'products'],
          notes: [
            '- **Cette route permet de récupérer tous les produits fournisseurs**\n' +
            '- En cas de succès, renvoie l\'ensemble des produits'
          ]
        }
      },
      {
        method: 'GET',
        path: '/products/{id}',
        config: {
          handler: productsController.findProductById,
          tags: ['api', 'products'],
          notes: [
            '- **Cette route permet de récupérer le produit sélectionné par l\' id**\n' +
            '- En cas de succès, renvoie le produit'
          ]
        }
      }
    ])
  }
}
