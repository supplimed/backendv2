import usecases from '../../../domain/usecases'
import productSerializer from '../../serializers/product-serializer'
import { Product } from '../../../domain/models/Product'
import { NotFoundError } from '../../../domain/errors'
import { PaginationInterface } from '../../../domain/usecases/find-products'
import * as _ from 'lodash'

function _extractPaginationFromQueryParams ({ page, pageSize }): {page: number, pageSize: number} {
  return {
    page: _.parseInt(page, 10),
    pageSize: _.parseInt(pageSize, 10)
  }
}

export default {
  async findProducts (request): Promise<{ data: Product[], pagination: PaginationInterface }> {
    const { page, pageSize }: { page: number, pageSize: number } = _extractPaginationFromQueryParams(request.query)
    const { paginatedProducts, pagination }: { paginatedProducts: Product[], pagination: PaginationInterface } = await usecases.findProducts({ page, pageSize })
    return productSerializer.serializeCollection(paginatedProducts, pagination)
  },

  async findProductById (request, h): Promise<{data: Product}> {
    try {
      const productId = request.params.id
      const product: Product = await usecases.findProductById({ productId })
      return productSerializer.serializeData(product)
    } catch (err) {
      if (err instanceof NotFoundError) {
        return h.response(err.toString()).code(401)
      }
    }
  }
}
