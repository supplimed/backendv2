import usecases from '../../../domain/usecases'
import { User } from '../../../domain/models/User'
import securityService = require('../../../domain/services/security-service')

export default {
  async loginUser (request, h): Promise<{user: User, token: string}> {
    const { email, password } = request.payload

    try {
      const user = await usecases.logUserIn({ email, password })
      const token = securityService.createTokenFromUser(user)
      return { user, token }
    } catch (err) {
      return h.response(err.toString()).code(err.errorCode || 400)
    }
  },

  async signupUser (request, h): Promise<{user: User, token: string}> {
    const { firstName, lastName, email, password, passwordConfirmation } = request.payload

    try {
      const user = await usecases.signupUser({ firstName, lastName, email, password, passwordConfirmation })
      const token = securityService.createTokenFromUser(user)
      return h.response({ user, token }).code(201)
    } catch (err) {
      return h.response({ msg: 'Signup failed' }).code(400)
    }
  }
}
