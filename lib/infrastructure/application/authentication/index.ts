'use strict'
import authenticationController from './authentication-controller'

export const authenticationPlugin = {
  name: 'authentication-api',
  version: '1.0.0',
  register: async function (server): Promise<void> {
    server.route([
      {
        method: 'POST',
        path: '/login',
        config: {
          handler: authenticationController.loginUser,
          tags: ['api', 'authentication'],
          notes: [
            '- **Cette route permet de login un nouvel utilisateur**\n' +
            '- En cas de succès, renvoie un tokenSecret signé avec les informations de l\'utilisateur'
          ]
        }
      },
      {
        method: 'POST',
        path: '/signup',
        config: {
          handler: authenticationController.signupUser,
          tags: ['api', 'authentication'],
          notes: [
            '- **Cette route permet de signup un nouvel utilisateur**\n' +
            '- En cas de succès, renvoie un 201'
          ]
        }
      }
    ])
  }
}
