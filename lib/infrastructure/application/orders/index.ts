import ordersController from './orders-controller'

export const orderPlugin = {
  name: 'orders-api',
  version: '1.0.0',
  register: async function (server): Promise<void> {
    server.route([
      {
        method: 'GET',
        path: '/basket/me',
        config: {
          handler: ordersController.getUserBasket,
          tags: ['api', 'orders'],
          notes: [
            '- **Cette route permet de récupérer son panier**\n' +
            '- En cas de succès, renvoie le panier de l\'utilisateur'
          ]
        }
      },
      {
        method: 'POST',
        path: '/basket/products/add',
        config: {
          handler: ordersController.addProductsToBasket,
          tags: ['api', 'orders'],
          notes: [
            '- **Cette route permet d\'ajouter un ou plusieurs produits au panier**\n' +
          '- En cas de succès, renvoie le panier de l\'utilisateur'
          ]
        }
      },
      {
        method: 'PUT',
        path: '/basket/products/set',
        config: {
          handler: ordersController.setProductQuantity,
          tags: ['api', 'orders'],
          notes: [
            '- **Cette route permet de modifier la quantité d\'un produit du panier**\n' +
          '- En cas de succès, renvoie le panier de l\'utilisateur'
          ]
        }
      },
      {
        method: 'PUT',
        path: '/basket/products/delete',
        config: {
          handler: ordersController.deleteProducts,
          tags: ['api', 'orders'],
          notes: [
            '- **Cette route permet de modifier de retirer tous les produits du panier**\n' +
          '- En cas de succès, renvoie un panier vide'
          ]
        }
      },
      {
        method: 'PUT',
        path: '/order/address',
        config: {
          handler: ordersController.updateOrderAddress,
          tags: ['api', 'orders'],
          notes: [
            '- **Cette route permet d\'ajouter une adresse de livraison à une commande**\n' +
            '- En cas de succès, renvoie un order'
          ]
        }
      },
      {
        method: 'GET',
        path: '/order',
        config: {
          handler: ordersController.getUserOrder,
          tags: ['api', 'orders'],
          notes: [
            '- **Cette route permet de récupérer une commande**\n' +
            '- En cas de succès, un status 0'
          ]
        }
      },
      {
        method: 'GET',
        path: '/order/history',
        config: {
          handler: ordersController.getUserOrderHistory,
          tags: ['api', 'orders'],
          notes: [
            '- **Cette route permet de récupérer l\'historique des commandes d\'un utilisateur**\n' +
            '- En cas de succès, renvoie l\'historique des commandes'
          ]
        }
      },
      {
        method: 'GET',
        path: '/order/history/{id}',
        config: {
          handler: ordersController.getUserOrderHistoryById,
          tags: ['api', 'orders'],
          notes: [
            '- **Cette route permet de récupérer une commande en particulier**\n' +
            '- En cas de succès, renvoie l\'historique des commandes'
          ]
        }
      }
    ])
  }
}
