import usecases from '../../../domain/usecases'
import { Basket } from '../../../domain/models/Basket'
import * as securityService from '../../../domain/services/security-service'
import { Order } from '../../../domain/models/Order'
import { OrderHistory } from '../../../domain/models/OrderHistory'

export default {
  async addProductsToBasket (request, h): Promise<Basket> {
    let id
    try {
      const token = request.headers.authorization.split(/Bearer /i)[1]
      id = await securityService.extractInfoFromToken(token)
    } catch (err) {
      return h.response({ msg: 'Invalid token' }).code(401)
    }

    try {
      const { products } = request.payload
      const basket: Basket = await usecases.addProductsToBasket({ id, products })
      return h.response(basket)
    } catch (err) {
      return h.response(err.toString()).code(err.errorCode || 404)
    }
  },

  async deleteProducts (request, h): Promise<boolean> {
    let id
    try {
      const token = request.headers.authorization.split(/Bearer /i)[1]
      id = await securityService.extractInfoFromToken(token)
    } catch (err) {
      return h.response({ msg: 'Invalid token' }).code(401)
    }

    try {
      return await usecases.deleteProductsFromBasket({ id })
    } catch (err) {
      return h.response(err.toString()).code(err.errorCode || 404)
    }
  },

  async getUserBasket (request, h): Promise<Basket> {
    let id
    try {
      const token = request.headers.authorization.split(/Bearer /i)[1]
      id = await securityService.extractInfoFromToken(token)
    } catch (err) {
      return h.response({ msg: 'Invalid token' }).code(401)
    }

    try {
      return await usecases.getUserBasket({ id })
    } catch (err) {
      h.response({ msg: 'usecases getUserBasket failed' }).code(401)
    }
  },

  async setProductQuantity (request, h): Promise<Basket> {
    let id
    try {
      const token = request.headers.authorization.split(/Bearer /i)[1]
      id = await securityService.extractInfoFromToken(token)
    } catch (err) {
      return h.response({ msg: 'Invalid token' }).code(401)
    }

    try {
      const { products } = request.payload
      return await usecases.setProductQuantity({ id, products })
    } catch (err) {
      return h.response(err.toString()).code(err.errorCode || 404)
    }
  },

  async updateOrderAddress (request, h) {
    let id
    try {
      const token = request.headers.authorization.split(/Bearer /i)[1]
      id = await securityService.extractInfoFromToken(token)
    } catch (err) {
      return h.response({ msg: 'Invalid token' }).code(401)
    }
    const { deliveryAddress, billingAddress } = request.payload

    let res
    try {
      res = await usecases.updateOrderAddress({ id, deliveryAddress, billingAddress })
    } catch (err) {
      return h.response({ errorDescription: err.message || 'usecases updateOrderAddress failed', status: 1 || 2 }).code(err.errorCode || 500)
    }
    return h.response(res)
  },

  async getUserOrder (request, h): Promise<string> {
    let id
    try {
      const token = request.headers.authorization.split(/Bearer /i)[1]
      id = await securityService.extractInfoFromToken(token)
    } catch (err) {
      return h.response({ msg: 'Invalid token' }).code(401)
    }

    // Get the order
    let order: Order
    try {
      order = await usecases.getUserOrder({ id })
    } catch (err) {
      return h.response({ errorDescription: err.message || 'usecases getUserOrder failed', status: 1 || 2 }).code(err.errorCode || 500)
    }

    // Get the basket
    let basket: Basket
    try {
      basket = await usecases.getUserBasket({ id })
    } catch (err) {
      return h.response({ errorDescription: 'Couldn\t get the basket', status: 1 }).code(401)
    }

    order.basket = basket
    return h.response({ order: order, status: 0 })
  },

  async getUserOrderHistory (request, h): Promise<string> {
    let id
    try {
      const token = request.headers.authorization.split(/Bearer /i)[1]
      id = await securityService.extractInfoFromToken(token)
    } catch (err) {
      return h.response({ msg: 'Invalid token' }).code(401)
    }

    // Get the order
    let orderHistory: OrderHistory[]
    try {
      orderHistory = await usecases.getUserOrderHistory({ id })
    } catch (err) {
      return h.response({ errorDescription: err.message || 'usecases getUserOrderHistory failed', status: 1 || 2 }).code(err.errorCode || 500)
    }

    return h.response({ orderHistory: orderHistory, status: 0 })
  },

  async getUserOrderHistoryById (request, h): Promise<string> {
    let id
    try {
      const token = request.headers.authorization.split(/Bearer /i)[1]
      id = await securityService.extractInfoFromToken(token)
    } catch (err) {
      return h.response({ msg: 'Invalid token' }).code(401)
    }
    const orderHistoryId: string = request.params.id

    // Get the order
    let orderHistory: OrderHistory
    try {
      orderHistory = await usecases.getUserOrderHistoryById({ id: id, orderHistoryId: orderHistoryId })
    } catch (err) {
      return h.response({ errorDescription: err.message || 'usecases getUserOrderHistoryById failed', status: 1 || 2 }).code(err.errorCode || 500)
    }

    return h.response({ orderHistory: orderHistory, status: 0 })
  }
}
