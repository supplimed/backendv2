import usersController from './users-controller'

export const usersPlugin = {
  name: 'users-api',
  version: '1.0.0',
  register: async function (server): Promise<void> {
    server.route([
      {
        method: 'GET',
        path: '/users/me',
        config: {
          handler: usersController.getCurrentUser,
          tags: ['api', 'users'],
          notes: [
            '- **Cette route permet de récupérer son profil**\n' +
            '- En cas de succès, renvoie le profil complet de l\'utilisateur'
          ]
        }
      },
      {
        method: 'PATCH',
        path: '/users/{id}',
        config: {
          handler: usersController.updateUserProfile,
          tags: ['api', 'users'],
          notes: [
            '- **Cette route permet de modifier le profil d\'un utilisateur**\n' +
            '- En cas de succès, renvoie le profil complet mis à jour de l\'utilisateur'
          ]
        }
      },
      {
        method: 'PUT',
        path: '/users/{id}/password',
        config: {
          handler: usersController.updateUserPassword,
          tags: ['api', 'users'],
          notes: [
            '- **Cette route permet de modifier le mot de passe d\'un utilisateur**\n' +
            '- En cas de succès, renvoie simplement un 204.'
          ]
        }
      },
      {
        method: 'POST',
        path: '/users/address/add',
        config: {
          handler: usersController.addUserAddress,
          tags: ['api', 'users'],
          notes: [
            '- **Cette route permet d\'ajouter une adresse**\n' +
            '- En cas de succès, renvoie le user'
          ]
        }
      }
    ])
  }
}
