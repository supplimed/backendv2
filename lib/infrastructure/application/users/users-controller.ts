import usecases from '../../../domain/usecases'
import {
  AddressAlreadySaved,
  AddressIsEmpty,
  EmailInvalidError,
  ForbiddenActionError
} from '../../../domain/errors'
import userSerializer from '../../serializers/user-serializer'
import * as securityService from '../../../domain/services/security-service'
import { User } from '../../../domain/models/User'

export default {
  async getCurrentUser (request, h): Promise<User> {
    let userId
    try {
      const token = request.headers.authorization.split(/Bearer /i)[1]
      userId = await securityService.extractInfoFromToken(token)
    } catch (err) {
      return h.response({ msg: 'Invalid token' }).code(401)
    }

    return usecases.getCurrentUser({ userId })
      .then(user => userSerializer.serialize(user))
  },

  async updateUserProfile (request, h): Promise<User> {
    const updatedUserId = request.params.id
    const { newEmail, newEmailConfirmation } = request.payload
    let userId
    try {
      const token = request.headers.authorization.split(/Bearer /i)[1]
      userId = await securityService.extractInfoFromToken(token)
    } catch (err) {
      return h.response({ msg: 'Invalid token' }).code(401)
    }

    return usecases.updateUserEmail({ userId, updatedUserId, newEmail, newEmailConfirmation })
      .then(user => userSerializer.serialize(user))
      .catch((err) => {
        if (err instanceof ForbiddenActionError) {
          return h.response({ msg: 'Forbidden' }).code(403)
        }
        if (err instanceof EmailInvalidError) {
          return h.response({ msg: `New email (${err.invalidEmail}) does not look like an email.` }).code(400)
        }
        throw err
      })
  },

  async updateUserPassword (request, h): Promise<User> {
    const updatedUserId = request.params.id
    const { oldPassword, newPassword, newPasswordConfirmation } = request.payload
    let userId

    try {
      const token = request.headers.authorization.split(/Bearer /i)[1]
      userId = await securityService.extractInfoFromToken(token)
    } catch (err) {
      return h.response({ msg: 'Invalid token' }).code(401)
    }

    return usecases.updateUserPassword({ userId, updatedUserId, oldPassword, newPassword, newPasswordConfirmation })
      .then(() => h.response().code(204))
      .catch(() => h.response({ msg: 'Password update failed' }).code(404))
  },

  async addUserAddress (request, h): Promise<User> {
    let id
    try {
      const token = request.headers.authorization.split(/Bearer /i)[1]
      id = await securityService.extractInfoFromToken(token)
    } catch (err) {
      return h.response({ msg: 'Invalid token' }).code(401)
    }
    const { address } = request.payload

    if (!address) {
      return h.response({ errorDescription: new AddressIsEmpty().message, status: 1 }).code(400)
    }

    try {
      await usecases.addUserAddress({ id, address })
    } catch (err) {
      if (err instanceof AddressAlreadySaved) { return h.response({ errorDescription: err.message, status: 1 }).code(400) }
      return h.response({ errorDescription: 'usecases addUserAddress failed', status: 1 }).code(500)
    }
    return usecases.getCurrentUser({ userId: id })
  }
}
