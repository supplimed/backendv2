const DEFAULT_PORT = 3000

interface ConfigI {
  apiVersion: string
  server: {
    port: number
    environment: string
  }
  auth: {
    tokenLifespan: string
    tokenSecret: string
    passwordSalt: number
  }
  idGenerator: {
    domain: string[]
    algorithm: string
  }
}

export default (function (): ConfigI {
  const config = {

    apiVersion: '1',

    server: {
      port: parseInt(process.env.SERVER_PORT, 10) || DEFAULT_PORT,
      environment: (process.env.NODE_ENV || 'development')
    },

    auth: {
      tokenLifespan: process.env.TOKEN_LIFESPAN || '1d', // https://github.com/zeit/ms
      tokenSecret: process.env.TOKEN_SECRET,
      passwordSalt: 10
    },

    idGenerator: {
      domain: '1234567890'.split(''),
      algorithm: 'aes-256-cbc'
    }
  }

  if (process.env.NODE_ENV === 'test') {
    config.auth.tokenSecret = 'dragon ball z'
  }

  return config
})()
