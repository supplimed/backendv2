// BUSINESS RULES

interface BusinessI {
  MAX_LENGTH_FOR_ID: number
  MAX_PRODUCTS_BY_ORDER: number
  MAX_QUANTITY_BY_PRODUCT: number
  MAX_LENGTH_FOR_STRING: number
  MIN_LENGTH_FOR_STRING: number
}

export default (function (): BusinessI {
  const business = {
    MAX_LENGTH_FOR_ID: 40,
    MAX_PRODUCTS_BY_ORDER: 99,
    MAX_QUANTITY_BY_PRODUCT: 999,
    MAX_LENGTH_FOR_STRING: 30,
    MIN_LENGTH_FOR_STRING: 1
  }

  return business
})()
