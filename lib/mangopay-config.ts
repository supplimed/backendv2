// MANGO PAY

import * as MangoPay from 'mangopay2-nodejs-sdk'

export default (function (): MangoPay {
  const mangoPay = new MangoPay({
    clientId: 'idclient100000',
    clientApiKey: 'axEdBK3k46mUsOJMGmo8BrCMgmyR8zosBb6kBXRxLy9B6qi12Z',
    // Set the right production API url. If testing, omit the property since it defaults to sandbox URL
    baseUrl: 'https://api.sandbox.mangopay.com'
  })

  return mangoPay
})()
