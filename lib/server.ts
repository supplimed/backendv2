'use strict'
import config from './config'
import routes from './routes'
import * as Hapi from '@hapi/hapi'
import * as HapiSwagger from 'hapi-swagger'
import * as Inert from '@hapi/inert'
import * as Vision from '@hapi/vision'

export async function createServer () {
  let serverConfig

  serverConfig = {
    port: config.server.port,
    host: 'localhost'
  }

  if (process.env.NODE_ENV === 'test') {
    serverConfig = {
      ...serverConfig,
      routes: {
        cors: true // Adding cors for communication with a local front
      }
    }
  }

  const server = Hapi.server(serverConfig)

  const swaggerOptions: HapiSwagger.RegisterOptions = {
    info: {
      title: 'Supplimed API Documentation'
    }
  }

  const plugins: Array<Hapi.ServerRegisterPluginObject<any>> = [
    {
      plugin: Inert
    },
    {
      plugin: Vision
    },
    {
      plugin: HapiSwagger,
      options: swaggerOptions
    }
  ]

  await server.register(plugins)
  await server.register(routes.unauthenticatedPlugins)
  await server.register(routes.authenticationPlugin, { routes: { prefix: '/authentication' } })
  await server.register(routes.authenticatedPlugins, { routes: { prefix: `/api/v${config.apiVersion}` } })

  if (process.env.NODE_ENV === 'test') {
    await server.register(routes.testPlugin)
  }

  return server
}
