import { DatabaseUser } from '../models/DatabaseUser'
import { DatabaseBasket } from '../models/DatabaseBasket'
import { DatabaseProduct } from '../models/DatabaseProduct'
import { DatabaseOrder } from '../models/DatabaseOrder'
import { DatabaseHistoryOrderHistory } from '../models/history/DatabaseHistoryOrderHistory'
import { Product } from '../../../lib/domain/models/Product'
import { User } from '../../../lib/domain/models/User'
import { DatabaseData } from '../models/DatabaseData'
import productsSeed from '../seeds/products'
import usersSeed from '../seeds/users'
import basketsSeed from '../seeds/baskets'
import orderSeed from '../seeds/orders'
import orderHistorySeed from '../seeds/ordersHistory'

import * as fs from 'mz/fs'
import * as path from 'path'
import * as bcrypt from 'bcrypt'
import * as _ from 'lodash'

import {
  DatabaseAlreadyEmptyError,
  DatabaseAlreadyExistsError,
  DatabaseConnectionFailedError,
  DatabaseCorruptionError,
  DatabaseDoesNotExistError,
  DatasourceNotFoundError,
  DatabaseAlreadyInitializedError
} from './database-errors'

const INIT_DATABASE = {
  __databaseName: 'Filesystem Database'
}

const EMPTY_DATABASE = {
  ...INIT_DATABASE,
  ...{
    users: [],
    products: [],
    baskets: [],
    orders: [],
    ordersHistory: []
  }
}

interface DatabaseI {
  users?: DatabaseUser[]
  products?: DatabaseProduct[]
  baskets?: DatabaseBasket[]
  orders?: DatabaseOrder[]
  ordersHistory?: DatabaseHistoryOrderHistory[]
}

class FilesystemDatabase {
  _databasePath

  constructor () {
    this._databasePath = path.join(__dirname, '/../data/database.json')
  }

  // #region Scripts functions

  async createDatabase (): Promise<void> {
    if (!await this._databaseIsInitialized()) {
      throw new DatabaseAlreadyInitializedError()
    }
    return this._writeData(INIT_DATABASE)
  }

  async dropDatabase (): Promise<void> {
    const dbInitialized = await this._databaseIsInitialized()
    if (dbInitialized) {
      throw new DatabaseDoesNotExistError()
    }
    return this._writeData(INIT_DATABASE)
  }

  async emptyDatabase (): Promise<void> {
    if (await this._databaseIsEmpty()) {
      throw new DatabaseAlreadyEmptyError()
    }
    return this._writeData(EMPTY_DATABASE)
  }

  async initDatabase (): Promise<void> {
    if (!await this._databaseIsInitialized()) {
      throw new DatabaseAlreadyExistsError()
    }
    return this._writeData(EMPTY_DATABASE)
  }

  async seedDatabase (): Promise<void> {
    if (!await this._databaseIsEmpty()) {
      throw new DatabaseAlreadyExistsError()
    }

    const database: DatabaseI = await this.getDatabase()

    for (const elem of usersSeed) {
      database.users.push(DatabaseUser.fromDatabaseUser(elem))
    }

    for (const elem of productsSeed) {
      database.products.push(DatabaseProduct.fromDatabaseProduct(elem))
    }

    for (const elem of basketsSeed) {
      database.baskets.push(DatabaseBasket.fromDatabaseBasket(elem))
    }

    for (const elem of orderSeed) {
      database.orders.push(DatabaseOrder.fromDatabaseOrder(elem))
    }

    for (const elem of orderHistorySeed) {
      database.ordersHistory.push(DatabaseHistoryOrderHistory.fromDatabaseOrderHistory(elem))
    }

    return this._writeData(database)
  }

  // #endregion

  // #region Filesystem functions

  private async _databaseIsInitialized (): Promise<boolean> {
    const database = await this.getDatabase()
    return _.isEqual(database, INIT_DATABASE)
  }

  private async _databaseIsEmpty (): Promise<boolean> {
    return this._databaseIsInit()
  }

  public async checkDatabaseIsEmpty (): Promise<boolean> {
    return this._databaseIsInit()
  }

  private async _databaseIsInit (): Promise<boolean> {
    const database = await this.getDatabase()
    return _.isEqual(database, EMPTY_DATABASE)
  }

  private _ensureCollectonExists (database: DatabaseI, collectionName: string): any {
    const collection = database[collectionName]
    if (!_.isArray(collection)) {
      throw new DatasourceNotFoundError()
    }
    return collection
  }

  async getCollectionByName (collectionName: string): Promise<any> {
    const database = await this.getDatabase()
    const collection = this._ensureCollectonExists(database, collectionName)
    return collection
  }

  private async getDatabase (): Promise<DatabaseI> {
    let database
    try {
      const databaseJSON = await fs.readFile(this._databasePath)
      try {
        database = JSON.parse(databaseJSON.toString())
        if (!_.isObject(database)) {
          throw new DatabaseCorruptionError()
        }
      } catch (err) {
        throw new DatabaseCorruptionError()
      }
    } catch (err) {
      throw new DatabaseConnectionFailedError()
    }
    return database
  }

  /**
   * Update an element of the database based on id
   * @param collectionName table we want to modify
   * @param itemId id of the item which must be updated
   * @param item updated item
   */
  private async updateItemById (collectionName: string, itemId: string, item: DatabaseData): Promise<void> {
    const collection = await this.getCollectionByName(collectionName)
    const idx = _.findIndex(collection, { id: itemId })
    collection[idx] = item
    const database = await this.getDatabase()
    database[collectionName] = collection
    return this._writeData(database)
  }

  private async _writeData (data: DatabaseData): Promise<void> {
    return fs.writeFile(this._databasePath, JSON.stringify(data, null, 4))
  }

  // #endregion

  // #region Business functions

  async addBasket (databaseBasket: DatabaseBasket): Promise<void> {
    const database: DatabaseI = await this.getDatabase()
    database.baskets.push(DatabaseBasket.fromDatabaseBasket(databaseBasket))
    return this._writeData(database)
  }

  async addProducts (products: Product[]): Promise<void> {
    const database: DatabaseI = await this.getDatabase()
    for (const p of products) {
      database.products.push(DatabaseProduct.fromProduct(p))
    }
    return this._writeData(database)
  }

  async addUser (user: DatabaseUser): Promise<void> {
    const database: DatabaseI = await this.getDatabase()
    database.users.push(DatabaseUser.fromDatabaseUser(user))
    return this._writeData(database)
  }

  async addOrder (databaseOrder: DatabaseOrder): Promise<void> {
    const database: DatabaseI = await this.getDatabase()
    database.orders.push(databaseOrder)
    return this._writeData(database)
  }

  async addOrderHistory (databaseOrderHistoryHistory: DatabaseHistoryOrderHistory): Promise<void> {
    const database: DatabaseI = await this.getDatabase()
    database.ordersHistory.push(databaseOrderHistoryHistory)
    return this._writeData(database)
  }

  async deleteBasket (id: string): Promise <void> {
    const database: DatabaseI = await this.getDatabase()
    database.baskets = database.baskets.filter(elem => elem.id !== id)
    return this._writeData(database)
  }

  async getItemById (collectionName: string, itemId: string): Promise<any> {
    const collection = await this.getCollectionByName(collectionName)
    return _.find(collection, { id: itemId })
  }

  async updateBasket (databaseBasket: DatabaseBasket): Promise<void> {
    await this.updateItemById('baskets', databaseBasket.id, databaseBasket)
  }

  async updateOrder (databaseOrder: DatabaseOrder): Promise<void> {
    await this.updateItemById('orders', databaseOrder.id, databaseOrder)
  }

  /**
   * Update attributes of an user in database
   * @param updatedDomainUser user with updated attributes
   */
  async updateUser (updatedDomainUser: User): Promise<DatabaseUser> {
    if (!updatedDomainUser.id) {
      throw new Error()
    }

    const currentDatabaseUser: DatabaseUser = await this.getItemById('users', updatedDomainUser.id)
    const newDatabaseUser: DatabaseUser = DatabaseUser.fromUser(updatedDomainUser, currentDatabaseUser.password)
    await this.updateItemById('users', newDatabaseUser.id, newDatabaseUser)

    return this.getItemById('users', newDatabaseUser.id)
  }

  /**
   * Update the password of an user in database
   * @param userId id of the user
   * @param password new password
   */
  async updateUserWithPassword (userId: string, password: string): Promise<DatabaseUser> {
    if (!userId) {
      throw new Error()
    }

    const databaseUser: DatabaseUser = await this.getItemById('users', userId)
    const currentPasswordSalt = 10
    databaseUser.password = bcrypt.hashSync(password, currentPasswordSalt)
    await this.updateItemById('users', databaseUser.id, databaseUser)

    return this.getItemById('users', databaseUser.id)
  }

  // #endregion
}

export const fsd = new FilesystemDatabase()
