export class DatabaseAlreadyEmptyError extends Error {
  constructor (message = 'Database already empty') {
    super(message)
    Object.setPrototypeOf(this, DatabaseAlreadyEmptyError.prototype)
  }
}

export class DatabaseAlreadyExistsError extends Error {
  constructor (message = 'Database already exists') {
    super(message)
    Object.setPrototypeOf(this, DatabaseAlreadyExistsError.prototype)
  }
}

export class DatabaseAlreadyInitializedError extends Error {
  constructor (message = 'Database already initiliazed') {
    super(message)
    Object.setPrototypeOf(this, DatabaseAlreadyInitializedError.prototype)
  }
}

export class DatabaseDoesNotExistError extends Error {
  constructor (message = 'Database does not exist') {
    super(message)
    Object.setPrototypeOf(this, DatabaseDoesNotExistError.prototype)
  }
}

export class DatabaseConnectionFailedError extends Error {
  constructor (message = 'Unable to open the data file') {
    super(message)
    Object.setPrototypeOf(this, DatabaseConnectionFailedError.prototype)
  }
}

export class DatabaseCorruptionError extends Error {
  constructor (message = 'The database is not in the right format') {
    super(message)
    Object.setPrototypeOf(this, DatabaseCorruptionError.prototype)
  }
}

export class DatasourceNotFoundError extends Error {
  constructor (message = 'The datasource was not found in the database') {
    super(message)
    Object.setPrototypeOf(this, DatasourceNotFoundError.prototype)
  }
}
