import { DatabaseUser } from '../models/DatabaseUser'

export default (function (): DatabaseUser[] {
  const usersSeed: DatabaseUser[] = [
    {
      id: '42',
      email: 'gogeta.denamek@dbz.com',
      password: '$2b$10$cx3cXA7Tg/phv3lCvrJ13.QBb3z9Z3EBkcxIFDazMq/67aaeq6Qwy', // 'supersayan'
      firstName: 'Gogeta',
      lastName: 'DeNamek',
      mpClientId: '80843674',
      mpWalletId: '80843675',
      mpCardId: ['81925645'],
      addresses: [
        {
          address: '1, rue du palmier',
          postalCode: '11111',
          city: 'ile de tortue génial'
        },
        {
          address: '2, rue du palmier',
          postalCode: '22222',
          city: 'ile de tortue génial'
        }
      ]
    },
    {
      id: '1337',
      email: 'booboo@dbz.com',
      password: '$2b$10$fw49wDluec99lnaBBNZTnufgKSc/BD7EvpO7ZdG2WUHBpXjXrjXHK', // 'babidi boo'
      firstName: 'Boo',
      lastName: 'Boo',
      mpClientId: null,
      mpWalletId: null,
      mpCardId: null
    }
  ]

  return usersSeed
})()
