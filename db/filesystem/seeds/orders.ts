import { DatabaseOrder } from '../models/DatabaseOrder'

export default (function (): DatabaseOrder[] {
  const orderSeed: DatabaseOrder[] = [
    {
      id: '42',
      deliveryAddress: null,
      billingAddress: null,
      basketId: '42'
    }
  ]
  return orderSeed
})()
