import { DatabaseBasket } from '../models/DatabaseBasket'

export default (function (): DatabaseBasket[] {
  const basketsSeed: DatabaseBasket[] = [
    {
      id: '42',
      databaseBasketElement: [
        {
          productId: '1',
          quantity: 1
        },
        {
          productId: '2',
          quantity: 2
        }
      ]
    },
    {
      id: '1337',
      databaseBasketElement: [
        {
          productId: '10',
          quantity: 11
        },
        {
          productId: '2',
          quantity: 3
        },
        {
          productId: '12',
          quantity: 6
        }
      ]
    }
  ]
  return basketsSeed
})()
