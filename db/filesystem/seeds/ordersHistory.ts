import { CURRENCIES } from '../../../lib/domain/models/Product'
import { DatabaseHistoryOrderHistory } from '../models/history/DatabaseHistoryOrderHistory'

export default (function (): DatabaseHistoryOrderHistory[] {
  const orderHistorySeed: DatabaseHistoryOrderHistory[] =
        [
          {
            id: '1111-111111-1111',
            date: 1594456603023,
            mpTransactionId: '83856801',
            userId: '42',
            order: {
              deliveryAddress: {
                address: '1, rue du palmier',
                postalCode: '11111',
                city: 'ile de tortue génial'
              },
              billingAddress: {
                address: '2, rue du palmier',
                postalCode: '22222',
                city: 'ile de tortue génial'
              },
              basket: {
                basketElements: [
                  {
                    product: {
                      id: '1',
                      name: 'Name for 1',
                      price: 1,
                      currency: CURRENCIES.EURO,
                      imageUrl: 'https://fake-img-server.com/url.jpg'
                    },
                    quantity: 1,
                    bulkPrice: 1
                  },
                  {
                    product: {
                      id: '2',
                      name: 'Name for 2',
                      price: 2,
                      currency: CURRENCIES.EURO,
                      imageUrl: 'https://fake-img-server.com/url.jpg'
                    },
                    quantity: 2,
                    bulkPrice: 4
                  }
                ],
                totalPrice: 5,
                totalProductsQuantity: 3
              }
            }
          },
          {
            id: '2222-222222-2222',
            date: 1594456603023,
            mpTransactionId: '83856801',
            userId: '42',
            order: {
              deliveryAddress: null,
              billingAddress: null,
              basket: {
                basketElements: [
                  {
                    product: {
                      id: '1',
                      name: 'Name for 1',
                      price: 1,
                      currency: CURRENCIES.EURO,
                      imageUrl: 'https://fake-img-server.com/url.jpg'
                    },
                    quantity: 1,
                    bulkPrice: 1
                  }
                ],
                totalPrice: 3000,
                totalProductsQuantity: 10
              }
            }
          }
        ]
  return orderHistorySeed
})()
