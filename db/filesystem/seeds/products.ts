import { DatabaseProduct } from '../models/DatabaseProduct'
import { CURRENCIES } from '../../../lib/domain/models/Product'

export default (function (): DatabaseProduct[] {
  const productsSeed: DatabaseProduct[] = [
    {
      id: '0',
      name: 'Name for 0',
      price: 0,
      currency: CURRENCIES.EURO,
      description: 'Description for 0',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '1',
      name: 'Name for 1',
      price: 1,
      currency: CURRENCIES.EURO,
      description: 'Description for 1',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '2',
      name: 'Name for 2',
      price: 2,
      currency: CURRENCIES.EURO,
      description: 'Description for 2',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '3',
      name: 'Name for 3',
      price: 3,
      currency: CURRENCIES.EURO,
      description: 'Description for 3',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '4',
      name: 'Name for 4',
      price: 4,
      currency: CURRENCIES.EURO,
      description: 'Description for 4',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '5',
      name: 'Name for 5',
      price: 5,
      currency: CURRENCIES.EURO,
      description: 'Description for 5',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '6',
      name: 'Name for 6',
      price: 6,
      currency: CURRENCIES.EURO,
      description: 'Description for 6',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '7',
      name: 'Name for 7',
      price: 7,
      currency: CURRENCIES.EURO,
      description: 'Description for 7',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '8',
      name: 'Name for 8',
      price: 8,
      currency: CURRENCIES.EURO,
      description: 'Description for 8',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '9',
      name: 'Name for 9',
      price: 9,
      currency: CURRENCIES.EURO,
      description: 'Description for 9',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '10',
      name: 'Name for 10',
      price: 10,
      currency: CURRENCIES.EURO,
      description: 'Description for 10',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '11',
      name: 'Name for 11',
      price: 11,
      currency: CURRENCIES.EURO,
      description: 'Description for 11',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '12',
      name: 'Name for 12',
      price: 12,
      currency: CURRENCIES.EURO,
      description: 'Description for 12',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '13',
      name: 'Name for 13',
      price: 13,
      currency: CURRENCIES.EURO,
      description: 'Description for 13',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '14',
      name: 'Name for 14',
      price: 14,
      currency: CURRENCIES.EURO,
      description: 'Description for 14',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '15',
      name: 'Name for 15',
      price: 15,
      currency: CURRENCIES.EURO,
      description: 'Description for 15',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '16',
      name: 'Name for 16',
      price: 16,
      currency: CURRENCIES.EURO,
      description: 'Description for 16',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '17',
      name: 'Name for 17',
      price: 17,
      currency: CURRENCIES.EURO,
      description: 'Description for 17',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '18',
      name: 'Name for 18',
      price: 18,
      currency: CURRENCIES.EURO,
      description: 'Description for 18',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '19',
      name: 'Name for 19',
      price: 19,
      currency: CURRENCIES.EURO,
      description: 'Description for 19',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '20',
      name: 'Name for 20',
      price: 20,
      currency: CURRENCIES.EURO,
      description: 'Description for 20',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '21',
      name: 'Name for 21',
      price: 21,
      currency: CURRENCIES.EURO,
      description: 'Description for 21',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '22',
      name: 'Name for 22',
      price: 22,
      currency: CURRENCIES.EURO,
      description: 'Description for 22',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '23',
      name: 'Name for 23',
      price: 23,
      currency: CURRENCIES.EURO,
      description: 'Description for 23',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '24',
      name: 'Name for 24',
      price: 24,
      currency: CURRENCIES.EURO,
      description: 'Description for 24',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '25',
      name: 'Name for 25',
      price: 25,
      currency: CURRENCIES.EURO,
      description: 'Description for 25',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '26',
      name: 'Name for 26',
      price: 26,
      currency: CURRENCIES.EURO,
      description: 'Description for 26',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '27',
      name: 'Name for 27',
      price: 27,
      currency: CURRENCIES.EURO,
      description: 'Description for 27',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '28',
      name: 'Name for 28',
      price: 28,
      currency: CURRENCIES.EURO,
      description: 'Description for 28',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '29',
      name: 'Name for 29',
      price: 29,
      currency: CURRENCIES.EURO,
      description: 'Description for 29',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '30',
      name: 'Name for 30',
      price: 30,
      currency: CURRENCIES.EURO,
      description: 'Description for 30',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '31',
      name: 'Name for 31',
      price: 31,
      currency: CURRENCIES.EURO,
      description: 'Description for 31',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '32',
      name: 'Name for 32',
      price: 32,
      currency: CURRENCIES.EURO,
      description: 'Description for 32',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '33',
      name: 'Name for 33',
      price: 33,
      currency: CURRENCIES.EURO,
      description: 'Description for 33',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '34',
      name: 'Name for 34',
      price: 34,
      currency: CURRENCIES.EURO,
      description: 'Description for 34',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '35',
      name: 'Name for 35',
      price: 35,
      currency: CURRENCIES.EURO,
      description: 'Description for 35',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '36',
      name: 'Name for 36',
      price: 36,
      currency: CURRENCIES.EURO,
      description: 'Description for 36',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '37',
      name: 'Name for 37',
      price: 37,
      currency: CURRENCIES.EURO,
      description: 'Description for 37',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '38',
      name: 'Name for 38',
      price: 38,
      currency: CURRENCIES.EURO,
      description: 'Description for 38',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '39',
      name: 'Name for 39',
      price: 39,
      currency: CURRENCIES.EURO,
      description: 'Description for 39',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '40',
      name: 'Name for 40',
      price: 40,
      currency: CURRENCIES.EURO,
      description: 'Description for 40',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '41',
      name: 'Name for 41',
      price: 41,
      currency: CURRENCIES.EURO,
      description: 'Description for 41',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '42',
      name: 'Name for 42',
      price: 42,
      currency: CURRENCIES.EURO,
      description: 'Description for 42',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '43',
      name: 'Name for 43',
      price: 43,
      currency: CURRENCIES.EURO,
      description: 'Description for 43',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '44',
      name: 'Name for 44',
      price: 44,
      currency: CURRENCIES.EURO,
      description: 'Description for 44',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '45',
      name: 'Name for 45',
      price: 45,
      currency: CURRENCIES.EURO,
      description: 'Description for 45',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '46',
      name: 'Name for 46',
      price: 46,
      currency: CURRENCIES.EURO,
      description: 'Description for 46',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '47',
      name: 'Name for 47',
      price: 47,
      currency: CURRENCIES.EURO,
      description: 'Description for 47',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '48',
      name: 'Name for 48',
      price: 48,
      currency: CURRENCIES.EURO,
      description: 'Description for 48',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '49',
      name: 'Name for 49',
      price: 49,
      currency: CURRENCIES.EURO,
      description: 'Description for 49',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '50',
      name: 'Name for 50',
      price: 50,
      currency: CURRENCIES.EURO,
      description: 'Description for 50',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '51',
      name: 'Name for 51',
      price: 51,
      currency: CURRENCIES.EURO,
      description: 'Description for 51',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '52',
      name: 'Name for 52',
      price: 52,
      currency: CURRENCIES.EURO,
      description: 'Description for 52',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '53',
      name: 'Name for 53',
      price: 53,
      currency: CURRENCIES.EURO,
      description: 'Description for 53',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '54',
      name: 'Name for 54',
      price: 54,
      currency: CURRENCIES.EURO,
      description: 'Description for 54',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '55',
      name: 'Name for 55',
      price: 55,
      currency: CURRENCIES.EURO,
      description: 'Description for 55',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '56',
      name: 'Name for 56',
      price: 56,
      currency: CURRENCIES.EURO,
      description: 'Description for 56',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '57',
      name: 'Name for 57',
      price: 57,
      currency: CURRENCIES.EURO,
      description: 'Description for 57',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '58',
      name: 'Name for 58',
      price: 58,
      currency: CURRENCIES.EURO,
      description: 'Description for 58',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '59',
      name: 'Name for 59',
      price: 59,
      currency: CURRENCIES.EURO,
      description: 'Description for 59',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '60',
      name: 'Name for 60',
      price: 60,
      currency: CURRENCIES.EURO,
      description: 'Description for 60',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '61',
      name: 'Name for 61',
      price: 61,
      currency: CURRENCIES.EURO,
      description: 'Description for 61',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '62',
      name: 'Name for 62',
      price: 62,
      currency: CURRENCIES.EURO,
      description: 'Description for 62',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '63',
      name: 'Name for 63',
      price: 63,
      currency: CURRENCIES.EURO,
      description: 'Description for 63',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '64',
      name: 'Name for 64',
      price: 64,
      currency: CURRENCIES.EURO,
      description: 'Description for 64',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '65',
      name: 'Name for 65',
      price: 65,
      currency: CURRENCIES.EURO,
      description: 'Description for 65',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '66',
      name: 'Name for 66',
      price: 66,
      currency: CURRENCIES.EURO,
      description: 'Description for 66',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '67',
      name: 'Name for 67',
      price: 67,
      currency: CURRENCIES.EURO,
      description: 'Description for 67',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '68',
      name: 'Name for 68',
      price: 68,
      currency: CURRENCIES.EURO,
      description: 'Description for 68',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '69',
      name: 'Name for 69',
      price: 69,
      currency: CURRENCIES.EURO,
      description: 'Description for 69',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '70',
      name: 'Name for 70',
      price: 70,
      currency: CURRENCIES.EURO,
      description: 'Description for 70',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '71',
      name: 'Name for 71',
      price: 71,
      currency: CURRENCIES.EURO,
      description: 'Description for 71',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '72',
      name: 'Name for 72',
      price: 72,
      currency: CURRENCIES.EURO,
      description: 'Description for 72',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '73',
      name: 'Name for 73',
      price: 73,
      currency: CURRENCIES.EURO,
      description: 'Description for 73',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '74',
      name: 'Name for 74',
      price: 74,
      currency: CURRENCIES.EURO,
      description: 'Description for 74',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '75',
      name: 'Name for 75',
      price: 75,
      currency: CURRENCIES.EURO,
      description: 'Description for 75',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '76',
      name: 'Name for 76',
      price: 76,
      currency: CURRENCIES.EURO,
      description: 'Description for 76',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '77',
      name: 'Name for 77',
      price: 77,
      currency: CURRENCIES.EURO,
      description: 'Description for 77',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '78',
      name: 'Name for 78',
      price: 78,
      currency: CURRENCIES.EURO,
      description: 'Description for 78',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '79',
      name: 'Name for 79',
      price: 79,
      currency: CURRENCIES.EURO,
      description: 'Description for 79',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '80',
      name: 'Name for 80',
      price: 80,
      currency: CURRENCIES.EURO,
      description: 'Description for 80',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '81',
      name: 'Name for 81',
      price: 81,
      currency: CURRENCIES.EURO,
      description: 'Description for 81',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '82',
      name: 'Name for 82',
      price: 82,
      currency: CURRENCIES.EURO,
      description: 'Description for 82',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '83',
      name: 'Name for 83',
      price: 83,
      currency: CURRENCIES.EURO,
      description: 'Description for 83',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '84',
      name: 'Name for 84',
      price: 84,
      currency: CURRENCIES.EURO,
      description: 'Description for 84',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '85',
      name: 'Name for 85',
      price: 85,
      currency: CURRENCIES.EURO,
      description: 'Description for 85',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '86',
      name: 'Name for 86',
      price: 86,
      currency: CURRENCIES.EURO,
      description: 'Description for 86',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '87',
      name: 'Name for 87',
      price: 87,
      currency: CURRENCIES.EURO,
      description: 'Description for 87',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '88',
      name: 'Name for 88',
      price: 88,
      currency: CURRENCIES.EURO,
      description: 'Description for 88',
      imageUrl: 'assets/images/potaras.png'
    },
    {
      id: '89',
      name: 'Name for 89',
      price: 89,
      currency: CURRENCIES.EURO,
      description: 'Description for 89',
      imageUrl: 'assets/images/potaras.png'
    }
  ]

  return productsSeed
})()
