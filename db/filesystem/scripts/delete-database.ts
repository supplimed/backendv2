import { DatabaseDoesNotExistError } from '../lib/database-errors'
import { success, info, error } from './script-utils'
import { fsd } from '../lib/filesystem-database'

fsd.dropDatabase()
  .then(() => success('Filesystem database deleted'))
  .catch((err) => {
    if (err instanceof DatabaseDoesNotExistError) {
      info('Filesystem database already deleted')
      process.exit(0)
    }
    error(err)
    process.exit(1)
  })
