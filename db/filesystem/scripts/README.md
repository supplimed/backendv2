In the context of a simple filesystem datastore : 

- CREATE = ensuring the file exists
- DELETE = ensuring collections are deleted
- EMPTY = emptying all datas from collections, structure unchanged
- INIT = ensuring collections exists
- MIGRATE = setting up the default empty structure in the filesystem
- SEED = inserting data for development purposes
