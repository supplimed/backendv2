import { DatabaseAlreadyEmptyError, DatabaseDoesNotExistError } from '../lib/database-errors'
import { success, info, error } from './script-utils'
import { fsd } from '../lib/filesystem-database'

fsd.emptyDatabase()
  .then(() => success('Filesystem database reset to empty state successfully'))
  .catch((err) => {
    if (err instanceof DatabaseAlreadyEmptyError) {
      info('Filesystem database already empty')
      process.exit(0)
    }
    if (err instanceof DatabaseDoesNotExistError) {
      error('Filesystem database does not exist')
      process.exit(1)
    }
    error(err)
    process.exit(1)
  })
