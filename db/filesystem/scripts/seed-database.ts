import { DatabaseConnectionFailedError, DatabaseAlreadyExistsError } from '../lib/database-errors'
import { success, info, error } from './script-utils'
import { fsd } from '../lib/filesystem-database'

fsd.seedDatabase()
  .then(() => success('Seed data applied successfully to the filesystem database'))
  .catch((err) => {
    if (err instanceof DatabaseAlreadyExistsError) {
      info('Filesystem database already created')
      process.exit(1)
    }
    if (err instanceof DatabaseConnectionFailedError) {
      error('Filesystem database corrupted. Should be initialized with {}')
      process.exit(0)
    }
    error(err)
    process.exit(1)
  })
