import * as chalk from 'chalk'

export function success (msg): void { console.log(chalk.green.bold('Success ') + msg) }
export function info (msg): void { console.log(chalk.blue.bold('Info ') + msg) }
export function error (err): void { console.log(chalk.red.bold('Error ') + err) }
