import { fsd as db } from '../lib/filesystem-database'

console.log('Starting...')
db.getCollectionByName('users')
  .then((users) => {
    console.log(users)
    console.log('...done')

    process.exit(0)
  })
  .catch((err) => {
    console.error(err)
    process.exit(1)
  })
