import { Address } from '../../../lib/domain/models/Address'
import { Order } from '../../../lib/domain/models/Order'

export class DatabaseOrder {
    public deliveryAddress: Address
    public billingAddress: Address
    public basketId: string
    public id: string

    /**
     * Create an DatabaseOrder from an Order
     * If there is no basket, we use the order id as basket id
     * @param order input order
     */
    static fromOrder (order: Order): DatabaseOrder {
      return new DatabaseOrder(order.id, order.deliveryAddress || null, order.id, order.billingAddress || null)
    }

    static fromDatabaseOrder (order: DatabaseOrder): DatabaseOrder {
      return new DatabaseOrder(order.id, order.deliveryAddress || null, order.id, order.billingAddress || null)
    }

    constructor (id: string, deliveryAddress: Address, basketId: string, billingAddress: Address) {
      this.id = id
      this.deliveryAddress = deliveryAddress || null
      this.billingAddress = billingAddress || null
      this.basketId = basketId || null
    }
}
