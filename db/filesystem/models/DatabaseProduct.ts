import { CURRENCIES, Product } from '../../../lib/domain/models/Product'
import { DatabaseData } from './DatabaseData'

export class DatabaseProduct extends DatabaseData {
  id: string
  price: number
  currency: CURRENCIES
  name: string
  description: string
  imageUrl: string

  static fromDatabaseProduct (dbProduct: DatabaseProduct): DatabaseProduct {
    return new DatabaseProduct(dbProduct.id, dbProduct.name, dbProduct.price, dbProduct.description, dbProduct.imageUrl, dbProduct.currency)
  }

  static fromProduct (product: Product): DatabaseProduct {
    return new DatabaseProduct(product.id, product.name, product.price, product.description, product.imageUrl, product.currency)
  }

  constructor (
    id: string,
    name: string,
    price: number,
    description: string,
    imageUrl: string,
    currency: CURRENCIES = CURRENCIES.EURO
  ) {
    super()
    this.id = id
    this.name = name
    this.price = price
    this.currency = currency
    this.description = description
    this.imageUrl = imageUrl
  }
}
