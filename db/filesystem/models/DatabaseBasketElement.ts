import { DatabaseData } from './DatabaseData'

export class DatabaseBasketElement extends DatabaseData {
  public productId: string
  public quantity: number

  static fromDatabaseBasketElement (dbBasketElement: DatabaseBasketElement): DatabaseBasketElement {
    return new DatabaseBasketElement(dbBasketElement.productId, dbBasketElement.quantity)
  }

  constructor (
    productId: string,
    quantity: number
  ) {
    super()
    this.productId = productId
    this.quantity = quantity
  }
}
