import { Address } from '../../../lib/domain/models/Address'
import { DatabaseData } from './DatabaseData'

export class DatabaseAddress extends DatabaseData {
    public address: string
    public postalCode: string
    public city: string

    static fromDatabaseAddress ({ address, city, postalCode }: DatabaseAddress): DatabaseAddress {
      return new Address(address, postalCode, city)
    }

    static fromAddress ({ address, city, postalCode }: Address): DatabaseAddress {
      return new Address(address, postalCode, city)
    }

    constructor (
      address: string,
      postalCode: string,
      city: string
    ) {
      super()
      this.address = address
      this.postalCode = postalCode
      this.city = city
    }
}
