import { DatabaseBasketElement } from './DatabaseBasketElement'
import { DatabaseData } from './DatabaseData'

export class DatabaseBasket extends DatabaseData {
  public id: string
  public databaseBasketElement: DatabaseBasketElement[]

  static fromDatabaseBasket (dbBasket: DatabaseBasket): DatabaseBasket {
    return new DatabaseBasket(dbBasket.id, dbBasket.databaseBasketElement)
  }

  constructor (
    id: string,
    databaseBasketElement: DatabaseBasketElement[]
  ) {
    super()
    this.id = id
    this.databaseBasketElement = databaseBasketElement
  }
}
