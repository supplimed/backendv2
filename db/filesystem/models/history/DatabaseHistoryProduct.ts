import { DatabaseData } from '../DatabaseData'
import { CURRENCIES, Product } from '../../../../lib/domain/models/Product'

export class DatabaseHistoryProduct extends DatabaseData {
  public id: string
  public name: string
  public price: number
  public currency: CURRENCIES
  public imageUrl: string
  public type?: string

  static fromProduct (product: Product): DatabaseHistoryProduct {
    return new DatabaseHistoryProduct(product.id, product.name, product.price, product.description, product.imageUrl, product.currency)
  }

  constructor (id: string, name: string, price: number, description: string, imageUrl: string, currency: CURRENCIES = CURRENCIES.EURO) {
    super()
    this.id = id
    this.name = name
    this.price = price
    this.currency = currency || CURRENCIES.EURO
    this.imageUrl = imageUrl
  }
}
