import { DatabaseHistoryBasketElement } from './DatabaseHistoryBasketElement'
import { DatabaseData } from '../DatabaseData'
import { Basket } from '../../../../lib/domain/models/Basket'
import _ = require('lodash')

export class DatabaseHistoryBasket extends DatabaseData {
  public basketElements: DatabaseHistoryBasketElement[]
  public totalPrice: number
  public totalProductsQuantity: number

  static fromBasket (basket: Basket): DatabaseHistoryBasket {
    // Create DatabaseBasketElementHistory array from BasketElement array
    const databaseBasketElementHistory: DatabaseHistoryBasketElement[] = []
    basket.basketElements.forEach(element => databaseBasketElementHistory.push(DatabaseHistoryBasketElement.fromBasketElement(element)))

    return new DatabaseHistoryBasket(databaseBasketElementHistory)
  }

  constructor (basketElements: DatabaseHistoryBasketElement[]) {
    super()
    this.basketElements = basketElements
    this.totalPrice = _.sumBy(this.basketElements, 'bulkPrice') * 100
    this.totalProductsQuantity = _.sumBy(this.basketElements, 'quantity')
  }
}
