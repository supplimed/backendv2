import { DatabaseHistoryBasket } from './DatabaseHistoryBasket'
import { DatabaseAddress } from '../DatabaseAddress'
import { DatabaseData } from '../DatabaseData'
import { Order } from '../../../../lib/domain/models/Order'

export class DatabaseHistoryOrder extends DatabaseData {
    public basket: DatabaseHistoryBasket
    public deliveryAddress: DatabaseAddress
    public billingAddress: DatabaseAddress

    static fromOrder (order: Order): DatabaseHistoryOrder {
      return new DatabaseHistoryOrder(DatabaseHistoryBasket.fromBasket(order.basket), order.deliveryAddress, order.billingAddress)
    }

    constructor (basket: DatabaseHistoryBasket, deliveryAddress: DatabaseAddress, billingAddress: DatabaseAddress) {
      super()
      this.basket = basket || null
      this.deliveryAddress = deliveryAddress || null
      this.billingAddress = billingAddress || null
    }
}
