import { DatabaseHistoryOrder } from './DatabaseHistoryOrder'
import { DatabaseData } from '../DatabaseData'
import { OrderHistory } from '../../../../lib/domain/models/OrderHistory'

export class DatabaseHistoryOrderHistory extends DatabaseData {
    public id: string
    public order: DatabaseHistoryOrder
    public date: number
    public mpTransactionId: string
    public userId: string

    static fromOrderHistory (orderHistory: OrderHistory): DatabaseHistoryOrderHistory {
      return new DatabaseHistoryOrderHistory(orderHistory.id, orderHistory.userId, DatabaseHistoryOrder.fromOrder(orderHistory.order), orderHistory.date, orderHistory.mpTransactionId)
    }

    static fromDatabaseOrderHistory (orderHistory: DatabaseHistoryOrderHistory): DatabaseHistoryOrderHistory {
      return new DatabaseHistoryOrderHistory(orderHistory.id, orderHistory.userId, orderHistory.order, orderHistory.date, orderHistory.mpTransactionId)
    }

    constructor (id: string, userId: string, order: DatabaseHistoryOrder, date: number, mpTransactionId: string) {
      super()
      this.id = id
      this.order = order
      this.date = date
      this.mpTransactionId = mpTransactionId
      this.userId = userId
    }
}
