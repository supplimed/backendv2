import { DatabaseHistoryProduct } from './DatabaseHistoryProduct'
import { DatabaseData } from '../DatabaseData'
import { BasketElement } from '../../../../lib/domain/models/BasketElement'

export class DatabaseHistoryBasketElement extends DatabaseData {
  public product: DatabaseHistoryProduct
  public quantity: number
  public bulkPrice: number

  static fromBasketElement (basketElement: BasketElement): DatabaseHistoryBasketElement {
    return new DatabaseHistoryBasketElement(DatabaseHistoryProduct.fromProduct(basketElement.product), basketElement.quantity)
  }

  constructor (product: DatabaseHistoryProduct, quantity: number) {
    super()
    this.product = product
    this.quantity = quantity
    this.bulkPrice = this.quantity * this.product.price
  }
}
