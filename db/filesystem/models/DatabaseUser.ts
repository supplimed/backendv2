import { DatabaseData } from './DatabaseData'
import { User } from '../../../lib/domain/models/User'
import { DatabaseAddress } from './DatabaseAddress'

export class DatabaseUser extends DatabaseData {
  public id: string
  public email: string
  public password: string
  public firstName: string
  public lastName: string
  public mpClientId: string
  public mpWalletId: string
  public mpCardId: string[]
  public addresses?: DatabaseAddress[]

  static fromDatabaseUser (dbUser: DatabaseUser): DatabaseUser {
    return new DatabaseUser(
      dbUser.id,
      dbUser.email,
      dbUser.password,
      dbUser.firstName,
      dbUser.lastName,
      dbUser.mpClientId,
      dbUser.mpWalletId,
      dbUser.mpCardId,
      dbUser.addresses)
  }

  static fromUser (user: User, password: string): DatabaseUser {
    return new DatabaseUser(
      user.id,
      user.email,
      password,
      user.firstName,
      user.lastName,
      user.mpClientId,
      user.mpWalletId,
      user.mpCardId,
      user.addresses)
  }

  constructor (
    id: string,
    email: string,
    password: string,
    firstName: string,
    lastName: string,
    mpClientId?: string,
    mpWalletId?: string,
    mpCardId?: string[],
    deliveryAddress?: DatabaseAddress[]
  ) {
    super()
    this.id = id
    this.email = email
    this.password = password
    this.firstName = firstName
    this.lastName = lastName
    this.mpClientId = mpClientId || null
    this.mpWalletId = mpWalletId || null
    mpCardId ? this.mpCardId = mpCardId.slice() : this.mpCardId = null
    deliveryAddress ? this.addresses = deliveryAddress.slice() : this.addresses = null
  }
}
