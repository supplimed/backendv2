'use strict'
import { User } from '../../lib/domain/models/User'
import * as securityService from '../../lib/domain/services/security-service'
const isValidJWT = (token): boolean => /./.test(token)

describe('Security service', () => {
  describe('createTokenFromUser', () => {
    it('should be able to sign a valid json web token', function () {
      // given
      const user = new User('1', 'cell', 'Cell', 'Gero')
      // when
      const token = securityService.createTokenFromUser(user)
      // then
      expect(isValidJWT(token)).toBeTruthy()
    })
  })

  describe('extractInfoFromToken', () => {
    it('should be able to decode a json web token it self-signed', function () {
      // given
      const user = new User('1', 'cell', 'Cell', 'Gero')
      // when
      const token = securityService.createTokenFromUser(user)
      const decodedToken = securityService.extractInfoFromToken(token)
      // then
      expect(decodedToken).toBe('1')
    })
  })

  describe('isPasswordValid', () => {
    it('should return true if the password is not at least 8 characters long', () => {
      expect(securityService.isPasswordValid('12345678')).toBeTruthy()
      expect(securityService.isPasswordValid('012345678901234567890123456789')).toBeTruthy()
    })
    it('should return false if the password is not at least 8 characters long', () => {
      expect(securityService.isPasswordValid('1234567')).toBeFalsy()
    })
    it('should return false if the password is not at most 30 characters long', () => {
      expect(securityService.isPasswordValid('1234567891234567891234567891211')).toBeFalsy()
    })
  })
})
