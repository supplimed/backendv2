'use strict'
import { User } from '../../lib/domain/models/User'
import * as sinon from 'sinon'
import { createServer } from '../../lib/server'
import { DatabaseAlreadyEmptyError } from '../../db/filesystem/lib/database-errors'
import * as securityService from '../../lib/domain/services/security-service'
import { fsd as filesystemDatabase } from '../../db/filesystem'
import { addGogetaUserToDatabase, createGogetaDomainUser } from '../test-utils'

const apiToken = 'some token'

describe('Authentication', () => {
  let server

  beforeEach(async () => {
    try {
      await filesystemDatabase.emptyDatabase()
    } catch (err) {
      if (!(err instanceof DatabaseAlreadyEmptyError)) {
        throw err
      }
    }
    server = await createServer()
  })

  afterEach(async () => {
    await server.stop()
    sinon.restore()
  })

  describe('Login', () => {
    describe('POST /authentication/login', () => {
      it('should respond with 200 with a token when the user exists', async () => {
        // when
        const gogeta = await addGogetaUserToDatabase()
        const domainUser: User = User.fromDatabaseUser(gogeta.user)

        sinon.stub(securityService, 'createTokenFromUser').withArgs(domainUser).returns(apiToken)

        const res = await server.inject({
          method: 'post',
          url: '/authentication/login',
          payload: {
            email: 'gogeta.denamek@dbz.com',
            password: 'supersayan'
          }
        })

        // then
        expect(res.statusCode).toEqual(200)
        expect(res.result.token).toEqual(apiToken)
        expect(res.result.user).toEqual(domainUser)
      })
      it('should respond with 404 without a token when no email matches', async () => {
      // when
        const res = await server.inject({
          method: 'post',
          url: '/authentication/login',
          payload: {
            email: 'gogeta1995@dbz.com',
            password: 'supersayan'
          }
        })

        // then
        expect(res.statusCode).toEqual(404)
        expect(res.result.token).toBe(undefined)
      })
      it('should respond with 404 without a token when email matches but not the password', async () => {
      // when
        const res = await server.inject({
          method: 'post',
          url: '/authentication/login',
          payload: {
            email: 'gogeta.denamek@dbz.com',
            password: 'fakepassword'
          }
        })

        // then
        expect(res.statusCode).toEqual(404)
        expect(res.result.token).toBe(undefined)
      })
    })
  })

  describe('Signup', () => {
    describe('POST /authentication/signup', () => {
      it('should respond with 200 with a token when the user exists', async () => {
        // given
        const domainUser = createGogetaDomainUser()

        // when
        const res = await server.inject({
          method: 'post',
          url: '/authentication/signup',
          payload: {
            firstName: 'Gogeta',
            lastName: 'DeNamek',
            email: 'gogeta.denamek@dbz.com',
            password: 'supersayan',
            passwordConfirmation: 'supersayan'
          }
        })

        // then
        expect(res.statusCode).toEqual(201)
        expect(res.result.user.firstName).toEqual(domainUser.firstName)
        expect(res.result.user.lastName).toEqual(domainUser.lastName)
        expect(res.result.user.email).toEqual(domainUser.email)
        expect(res.result.user.id).toBeTruthy()
        expect(res.result.token).toBeTruthy()
      })
      it('should respond with 400 without a token when the user already exists', async () => {
        // when
        await server.inject({
          method: 'post',
          url: '/authentication/signup',
          payload: {
            email: 'bulma@dbz.com',
            password: 'bulmalovevegeta',
            passwordConfirmation: 'bulmalovevegeta'
          }
        })

        const res = await server.inject({
          method: 'post',
          url: '/authentication/signup',
          payload: {
            email: 'bulma@dbz.com',
            password: 'bulmalovevegeta',
            passwordConfirmation: 'bulmalovevegeta'
          }
        })

        // then
        expect(res.statusCode).toEqual(400)
      })
      it('should respond with 400 with a malformed email', async () => {
        // when
        const res = await server.inject({
          method: 'post',
          url: '/authentication/signup',
          payload: {
            email: 'bulma@',
            password: 'bulmalovevegeta',
            passwordConfirmation: 'bulmalovevegeta'
          }
        })

        // then
        expect(res.statusCode).toEqual(400)
      })
      it('should respond with 400 with an invalid password', async () => {
        // when
        const res = await server.inject({
          method: 'post',
          url: '/authentication/signup',
          payload: {
            email: 'bulma@dbz.com',
            password: 'trunk',
            passwordConfirmation: 'trunk'
          }
        })

        // then
        expect(res.statusCode).toEqual(400)
      })
    })
  })
})
