'use strict'
import { createServer } from '../../lib/server'

describe('Healthcheck', () => {
  let server

  beforeEach(async () => {
    server = await createServer()
  })
  afterEach(async () => {
    await server.stop()
  })

  describe('GET /status', () => {
    it('should respond with 200 and api version', async () => {
      const res = await server.inject({
        method: 'get',
        url: '/status'
      })
      expect(res.statusCode).toEqual(200)
      expect(res.result.version).toEqual('1')
    })
  })

  describe('GET /fake-url', () => {
    it('should respond with 404 when the route does not exist', async () => {
      const res = await server.inject({
        method: 'get',
        url: '/fake-url'
      })
      expect(res.statusCode).toEqual(404)
    })
  })
})
