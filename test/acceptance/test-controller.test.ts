'use strict'
import * as sinon from 'sinon'
import { createServer } from '../../lib/server'
import { DatabaseAlreadyEmptyError } from '../../db/filesystem/lib/database-errors'
import { fsd as filesystemDatabase } from '../../db/filesystem'

describe('Test', () => {
  let server

  beforeEach(async () => {
    try {
      await filesystemDatabase.emptyDatabase()
    } catch (err) {
      if (!(err instanceof DatabaseAlreadyEmptyError)) {
        throw err
      }
    }
    server = await createServer()
  })

  afterEach(async () => {
    await server.stop()
    sinon.restore()
  })

  describe('GET /test/reset', () => {
    it('should seed the database if the database is empty', async () => {
      const targetUsersCollection = [{
        id: '42',
        email: 'gogeta.denamek@dbz.com',
        password: '$2b$10$cx3cXA7Tg/phv3lCvrJ13.QBb3z9Z3EBkcxIFDazMq/67aaeq6Qwy',
        firstName: 'Gogeta',
        lastName: 'DeNamek',
        mpClientId: '80843674',
        mpWalletId: '80843675',
        mpCardId: ['81925645'],
        addresses: [
          {
            address: '1, rue du palmier',
            postalCode: '11111',
            city: 'ile de tortue génial'
          },
          {
            address: '2, rue du palmier',
            postalCode: '22222',
            city: 'ile de tortue génial'
          }
        ]
      },
      {
        id: '1337',
        email: 'booboo@dbz.com',
        password: '$2b$10$fw49wDluec99lnaBBNZTnufgKSc/BD7EvpO7ZdG2WUHBpXjXrjXHK',
        firstName: 'Boo',
        lastName: 'Boo',
        mpClientId: null,
        mpWalletId: null,
        mpCardId: null,
        addresses: null
      }]

      // when
      const res = await server.inject({
        method: 'GET',
        url: '/test/reset'
      })
      expect(await filesystemDatabase.getCollectionByName('users')).toEqual(targetUsersCollection)
      expect(res.statusCode).toEqual(200)
    })
  })

  it('should empty the database and seed it if the database is not empty', async () => {
    const targetUsersCollection = [{
      id: '42',
      email: 'gogeta.denamek@dbz.com',
      password: '$2b$10$cx3cXA7Tg/phv3lCvrJ13.QBb3z9Z3EBkcxIFDazMq/67aaeq6Qwy',
      firstName: 'Gogeta',
      lastName: 'DeNamek',
      mpClientId: '80843674',
      mpWalletId: '80843675',
      mpCardId: ['81925645'],
      addresses: [
        {
          address: '1, rue du palmier',
          postalCode: '11111',
          city: 'ile de tortue génial'
        },
        {
          address: '2, rue du palmier',
          postalCode: '22222',
          city: 'ile de tortue génial'
        }
      ]
    },
    {
      id: '1337',
      email: 'booboo@dbz.com',
      password: '$2b$10$fw49wDluec99lnaBBNZTnufgKSc/BD7EvpO7ZdG2WUHBpXjXrjXHK',
      firstName: 'Boo',
      lastName: 'Boo',
      mpClientId: null,
      mpWalletId: null,
      mpCardId: null,
      addresses: null
    }]

    const testUser = {
      id: '222',
      email: 'broly@dbz.com',
      password: '$2b$10$CNWp5NW6ya71xZS4/InMm.N4BNtAzF.1LTl/1DQGS.qzdqdqzdqz',
      firstName: 'Broly',
      lastName: 'LePlusFort',
      mpClientId: null,
      mpWalletId: null,
      mpCardId: null,
      addresses: null
    }

    await filesystemDatabase.addUser(testUser)
    expect(await filesystemDatabase.getCollectionByName('users')).toContainEqual(testUser)

    // when
    const res = await server.inject({
      method: 'GET',
      url: '/test/reset'
    })
    expect(await filesystemDatabase.getCollectionByName('users')).toEqual(targetUsersCollection)
    expect(res.statusCode).toEqual(200)
  })
})
