'use strict'
import { addProductstoDatabase, getSliceOfSerializedProduct, toTestProduct } from '../test-utils'
import { createServer } from '../../lib/server'
import { DatabaseAlreadyEmptyError } from '../../db/filesystem/lib/database-errors'
import productSerializer from '../../lib/infrastructure/serializers/product-serializer'
import { fsd as filesystemDatabase } from '../../db/filesystem'

describe('Products', () => {
  let server

  beforeEach(async () => {
    try {
      await filesystemDatabase.emptyDatabase()
    } catch (err) {
      if (!(err instanceof DatabaseAlreadyEmptyError)) {
        throw err
      }
    }
    server = await createServer()
  })

  afterEach(async () => {
    await server.stop()
  })

  describe('Products', () => {
    describe('GET /products', () => {
      it('should respond with empty list when they are no products', async () => {
        // given
        const products = await addProductstoDatabase(0)

        // when
        const res = await server.inject({
          method: 'get',
          url: '/products'
        })

        // then
        expect(res.statusCode).toBe(200)
        expect(res.result).toEqual({
          data: products,
          pagination: null
        })
      })
      it('should response with all products taking into account pagination defaults when none are provided ', async () => {
        // given
        const products = await addProductstoDatabase(90)
        const productPageSerialized = getSliceOfSerializedProduct(products, 0, 20)

        // when
        const res = await server.inject({
          method: 'get',
          url: '/products'
        })

        // then
        expect(res.statusCode).toBe(200)
        expect(res.result).toEqual({
          data: productPageSerialized,
          pagination: { totalCount: 90, pageCount: 5, pageSize: 20, page: 1 }
        })
      })
      it('should response with the products taking into account custom pagination options from query string', async () => {
        // given
        const products = await addProductstoDatabase(90)
        const productPageSerialized = getSliceOfSerializedProduct(products, 30, 60)

        // when
        const res = await server.inject({
          method: 'get',
          url: '/products?page=2&pageSize=30'
        })

        // then
        expect(res.statusCode).toBe(200)
        expect(res.result).toEqual({
          data: productPageSerialized,
          pagination: { totalCount: 90, pageCount: 3, pageSize: 30, page: 2 }
        })
      })
      it('should respond with the first page when the request is below lower boundary', async () => {
        // given
        const products = await addProductstoDatabase(90)
        const productPageSerialized = getSliceOfSerializedProduct(products, 0, 20)

        // when
        const res = await server.inject({
          method: 'get',
          url: '/products?page=-1&pageSize=20'
        })

        // then
        expect(res.statusCode).toBe(200)
        expect(res.result).toEqual({
          data: productPageSerialized,
          pagination: { totalCount: 90, pageCount: 5, pageSize: 20, page: 1 }
        })
      })
      it('should respond with a default page size when the request is above page size upper boundary', async () => {
        // given
        const products = await addProductstoDatabase(90)
        const productPageSerialized = getSliceOfSerializedProduct(products, 0, 50)

        // when
        const res = await server.inject({
          method: 'get',
          url: '/products?page=1&pageSize=2000'
        })

        // then
        expect(res.statusCode).toBe(200)
        expect(res.result).toEqual({
          data: productPageSerialized,
          pagination: { totalCount: 90, pageCount: 2, pageSize: 50, page: 1 }
        })
      })
      it('should respond with the last page when the request page is beyond the last one', async () => {
        // given
        const products = await addProductstoDatabase(90)
        const productPageSerialized = getSliceOfSerializedProduct(products, 80, 90)
        // when
        const res = await server.inject({
          method: 'get',
          url: '/products?page=100&pageSize=20'
        })

        // then
        expect(res.statusCode).toBe(200)
        expect(res.result).toEqual({
          data: productPageSerialized,
          pagination: { totalCount: 90, pageCount: 5, pageSize: 20, page: 5 }
        })
      })
    })

    describe('GET /products/{id}', () => {
      it('should respond with the product passed in the params', async () => {
        // given
        const productIdTarget = '10'
        const productTarget = toTestProduct({ currency: undefined, description: '', imageUrl: '', name: '', price: 0, id: productIdTarget })
        const serializedProduct = productSerializer.serializeData(productTarget)
        await addProductstoDatabase(90)

        // when
        const res = await server.inject({
          method: 'get',
          url: `/products/${productIdTarget}`
        })

        // then
        expect(res.statusCode).toBe(200)
        expect(res.result).toEqual(serializedProduct)
      })

      it('should respond with an error when product id is not in the database', async () => {
        // given
        const productIdTarget = 100
        await addProductstoDatabase(90)

        // when
        const res = await server.inject({
          method: 'get',
          url: `/products/${productIdTarget}`
        })

        // then
        expect(res.statusCode).toBe(401)
      })
    })
  })
})
