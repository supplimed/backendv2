'use strict'
import * as sinon from 'sinon'
import { createServer } from '../../lib/server'
import { DatabaseAlreadyEmptyError } from '../../db/filesystem/lib/database-errors'
import { fsd as filesystemDatabase } from '../../db/filesystem'
import {
  addGogetaUserToDatabase,
  getUserById,
  addGogetaUserWithMangoPayAttributesToDatabase,
  addBooUserWithMangoPayAttributesToDatabase,
  addProductstoDatabase,
  addBasketToDatabase,
  addGohanUserWithMangoPayAttributesToDatabase,
  addOrderToDatabase,
  toTestOrder,
  toTestBasket
} from '../test-utils'

describe('Payment', () => {
  let server

  beforeEach(async () => {
    try {
      await filesystemDatabase.emptyDatabase()
    } catch (err) {
      if (!(err instanceof DatabaseAlreadyEmptyError)) {
        throw err
      }
    }
    server = await createServer()
  })

  afterEach(async () => {
    await server.stop()
    sinon.restore()
  })

  describe('POST /api/v1/mangopay/createLegalUser', () => {
    it('should respond with a serialized mangopay user and wallet when token is ok', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()

      // when
      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/mangopay/createLegalUser',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          user: {
            Email: 'gogeta.denamek@dbz.com',
            LegalPersonType: 'BUSINESS',
            LegalRepresentativeBirthday: 1234567890,
            LegalRepresentativeCountryOfResidence: 'FR',
            LegalRepresentativeFirstName: 'Gogeta',
            LegalRepresentativeLastName: 'DeNamek',
            LegalRepresentativeNationality: 'FR',
            Name: 'Capsule Corporation'
          }
        }
      })

      const updatedGogeta = await getUserById(gogeta.user.id)

      // then
      expect(res.statusCode).toEqual(200)
      expect(res.result.user).toBeTruthy()
      expect(res.result.wallet).toBeTruthy()
      expect(res.result.status).toEqual(0)
      expect(res.result.wallet.Owners[0]).toEqual(res.result.user.Id)

      expect(updatedGogeta.mpClientId).toEqual(res.result.user.Id)
      expect(updatedGogeta.mpWalletId).toEqual(res.result.wallet.Id)
    })

    it('should respond with error 400 if the user already has a mangopay account', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()

      // when
      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/mangopay/createLegalUser',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          user: {
            Email: 'gogeta.denamek@dbz.com',
            LegalPersonType: 'BUSINESS',
            LegalRepresentativeBirthday: 1234567890,
            LegalRepresentativeCountryOfResidence: 'FR',
            LegalRepresentativeFirstName: 'Gogeta',
            LegalRepresentativeLastName: 'DeNamek',
            LegalRepresentativeNationality: 'FR',
            Name: 'Capsule Corporation'
          }
        }
      })

      const updatedGogeta = await getUserById(gogeta.user.id)

      // when
      const res2 = await server.inject({
        method: 'POST',
        url: '/api/v1/mangopay/createLegalUser',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          user: {
            Email: 'gogeta.denamek@dbz.com',
            LegalPersonType: 'BUSINESS',
            LegalRepresentativeBirthday: 1234567890,
            LegalRepresentativeCountryOfResidence: 'FR',
            LegalRepresentativeFirstName: 'Gogeta',
            LegalRepresentativeLastName: 'DeNamek',
            LegalRepresentativeNationality: 'FR',
            Name: 'Capsule Corporation'
          }
        }
      })

      // then
      expect(res.statusCode).toEqual(200)
      expect(updatedGogeta.mpClientId).toEqual(res.result.user.Id)
      expect(updatedGogeta.mpWalletId).toEqual(res.result.wallet.Id)

      expect(res2.statusCode).toEqual(400)
    })

    it('should respond with error 400 if we can\'t create the mangopay request\'s body', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()

      // when
      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/mangopay/createLegalUser',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          user: {
            Email: null,
            LegalPersonType: 'BUSINESS',
            LegalRepresentativeBirthday: 1234567890,
            LegalRepresentativeCountryOfResidence: 'FR',
            LegalRepresentativeFirstName: 'Gogeta',
            LegalRepresentativeLastName: 'DeNamek',
            LegalRepresentativeNationality: 'FR',
            Name: 'Capsule Corporation'
          }
        }
      })

      // then
      expect(res.statusCode).toEqual(400)
    })

    it('should respond with error 500 if MangoPay sends back an error', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()

      // when
      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/mangopay/createLegalUser',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          user: {
            Email: 'gogeta.denamek@dbz.com',
            LegalPersonType: 'wrongfield',
            LegalRepresentativeBirthday: 1234567890,
            LegalRepresentativeCountryOfResidence: 'FR',
            LegalRepresentativeFirstName: 'Gogeta',
            LegalRepresentativeLastName: 'DeNamek',
            LegalRepresentativeNationality: 'FR',
            Name: 'Capsule Corporation'
          }
        }
      })

      // then
      expect(res.statusCode).toEqual(500)
      expect(res.result.errors).toBeTruthy()
    })
  })

  describe('POST /api/v1/mangopay/cardregistration/create', () => {
    it('should respond with MangoPay card data when token is ok', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()

      // when
      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/mangopay/createLegalUser',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          user: {
            Email: 'gogeta.denamek@dbz.com',
            LegalPersonType: 'BUSINESS',
            LegalRepresentativeBirthday: 1234567890,
            LegalRepresentativeCountryOfResidence: 'FR',
            LegalRepresentativeFirstName: 'Gogeta',
            LegalRepresentativeLastName: 'DeNamek',
            LegalRepresentativeNationality: 'FR',
            Name: 'Capsule Corporation'
          }
        }
      })

      // when
      const res2 = await server.inject({
        method: 'GET',
        url: '/api/v1/mangopay/cardregistration/create',
        headers: { authorization: `Bearer ${gogeta.token}` }
      })

      // then
      expect(res.statusCode).toEqual(200)

      expect(res2.statusCode).toEqual(200)
      expect(res2.result.cardRegistrationData).toBeTruthy()
      expect(res2.result.cardRegistrationData.PreregistrationData).toBeTruthy()
      expect(res2.result.cardRegistrationData.AccessKey).toBeTruthy()
      expect(res2.result.cardRegistrationData.CardRegistrationURL).toBeTruthy()
      expect(res2.result.status).toEqual(0)
    })
  })

  describe('POST /api/v1/mangopay/cardregistration/save', () => {
    it('should respond with status 0 when token is ok', async () => {
      // given
      const gogeta = await addGogetaUserWithMangoPayAttributesToDatabase()

      // when
      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/mangopay/cardregistration/save',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          CardId: '12345678',
          UserId: '81455971'
        }
      })

      const updatedGogeta = await getUserById(gogeta.user.id)

      expect(res.statusCode).toEqual(200)
      expect(res.result.status).toEqual(0)
      expect(updatedGogeta.mpCardId).toEqual(['81455972', '12345678'])
    })

    it('should respond with error 500 when mpUserId doesn\'t match', async () => {
      // given
      const gogeta = await addGogetaUserWithMangoPayAttributesToDatabase()

      // when
      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/mangopay/cardregistration/save',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          CardId: '12345678',
          UserId: '00000000'
        }
      })

      const updatedGogeta = await getUserById(gogeta.user.id)

      expect(res.statusCode).toEqual(500)
      expect(res.result.status).toEqual(1)
      expect(updatedGogeta.mpCardId).toEqual(gogeta.user.mpCardId)
    })
  })

  describe('POST /api/v1//mangopay/payin/card/direct', () => {
    it('should respond status SUCCEEDED when the payIn is valid', async () => {
      // given
      const boo = await addBooUserWithMangoPayAttributesToDatabase()
      await addProductstoDatabase(10)
      await addBasketToDatabase(1, 5, boo.user.id)
      const order = toTestOrder(boo.user.id, null, null, toTestBasket(1, 5, boo.user.id))
      await addOrderToDatabase(order)

      // when
      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/mangopay/payin/card/direct',
        headers: { authorization: `Bearer ${boo.token}` },
        payload: {
          cardId: '81925645'
        }
      })

      expect(res.result.mpStatus).toEqual('SUCCEEDED')
      expect(res.result.resultCode).toEqual('000000')
      expect(res.result.orderHistoryId).toBeTruthy()
    })

    it('should respond status FAILED with the resultCode when the payIn is invalid', async () => {
      // given
      const boo = await addBooUserWithMangoPayAttributesToDatabase()

      // when
      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/mangopay/payin/card/direct',
        headers: { authorization: `Bearer ${boo.token}` },
        payload: {
          cardId: '81925645'
        }
      })
      expect(res.result.errorDescription).toEqual('Payment status is not SUCCEEDED')
    })
  })

  describe('GET /api/v1//mangopay/user/card', () => {
    it('should return all cards of the user', async () => {
      // given
      const gogeta = await addGogetaUserWithMangoPayAttributesToDatabase()

      // when
      const res = await server.inject({
        method: 'GET',
        url: '/api/v1/mangopay/user/cards',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
        }
      })
      expect(res.result.cards[0].expirationDate).toEqual('1020')
      expect(res.result.cards[0].alias).toEqual('497010XXXXXX4422')
      expect(res.result.cards[0].cardType).toEqual('CB_VISA_MASTERCARD')

      expect(res.result.cards[1].expirationDate).toEqual('1020')
      expect(res.result.cards[1].alias).toEqual('497248XXXXXX0049')
      expect(res.result.cards[1].cardType).toEqual('CB_VISA_MASTERCARD')
    })

    it('should return empty cards ', async () => {
      // given
      const gohan = await addGohanUserWithMangoPayAttributesToDatabase()

      // when
      const res = await server.inject({
        method: 'GET',
        url: '/api/v1/mangopay/user/cards',
        headers: { authorization: `Bearer ${gohan.token}` },
        payload: {
        }
      })
      expect(res.result.cards).toBeNull()
    })
  })
})
