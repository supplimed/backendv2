'use strict'
import * as sinon from 'sinon'
import { createServer } from '../../lib/server'
import { DatabaseAlreadyEmptyError } from '../../db/filesystem/lib/database-errors'
import { fsd as filesystemDatabase } from '../../db/filesystem'
import { Address } from '../../lib/domain/models/Address'

import {
  addBasketToDatabase,
  addGogetaUserToDatabase,
  addProductstoDatabase,
  toTestBasket, toTestDatabaseOrder,
  toTestOrder,
  addDatabaseOrderToDatabase,
  addBooUserWithMangoPayAttributesToDatabase,
  addOrderToDatabase
} from '../test-utils'
import { DatabaseOrder } from '../../db/filesystem/models/DatabaseOrder'

describe('Order', () => {
  let server

  beforeEach(async () => {
    try {
      await filesystemDatabase.emptyDatabase()
    } catch (err) {
      if (!(err instanceof DatabaseAlreadyEmptyError)) {
        throw err
      }
    }
    server = await createServer()
  })

  afterEach(async () => {
    await server.stop()
    sinon.restore()
  })

  describe('GET /api/v1/baskets/me', () => {
    it('should respond with a serialized basket when token is ok', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      await addProductstoDatabase(90)
      await addBasketToDatabase(1, 5, gogeta.user.id)
      const basket = toTestBasket(1, 5, gogeta.user.id)

      // when
      const res = await server.inject({
        method: 'GET',
        url: '/api/v1/basket/me',
        headers: { authorization: `Bearer ${gogeta.token}` }
      })

      // then
      expect(res.statusCode).toEqual(200)
      expect(res.result).toEqual(basket)
    })

    it('should respond with a serialized empty basket when the user has nothing in his basket and token is ok', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      await addProductstoDatabase(90)
      const basket = toTestBasket(0, 0, gogeta.user.id)

      // when
      const res = await server.inject({
        method: 'GET',
        url: '/api/v1/basket/me',
        headers: { authorization: `Bearer ${gogeta.token}` }
      })

      // then
      expect(res.statusCode).toEqual(200)
      expect(res.result).toEqual(basket)
    })
  })

  describe('GET /api/v1/baskets/products/add', () => {
    it('should respond with a serialized basket when one product is added to a new basket', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      await addProductstoDatabase(90)
      const basket = toTestBasket(1, 2, gogeta.user.id)

      // when
      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/basket/products/add',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          products: [{
            id: '1',
            quantity: 1
          }]
        }
      })

      // then
      expect(res.statusCode).toEqual(200)
      expect(res.result).toEqual(basket)
    })

    it('should respond with a serialized basket when several products are added to a new basket', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      await addProductstoDatabase(90)
      const basket = toTestBasket(1, 3, gogeta.user.id)

      // when
      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/basket/products/add',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          products: [{
            id: '1',
            quantity: 1
          },
          {
            id: '2',
            quantity: 2
          }]
        }
      })

      // then
      expect(res.statusCode).toEqual(200)
      expect(res.result).toEqual(basket)
    })

    it('should respond with a serialized basket when one product is added to an existing basket', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      await addProductstoDatabase(90)
      await addBasketToDatabase(1, 4, gogeta.user.id)
      const basket = toTestBasket(1, 5, gogeta.user.id)

      // when
      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/basket/products/add',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          products: [{
            id: '4',
            quantity: 4
          }]
        }
      })

      // then
      expect(res.statusCode).toEqual(200)
      expect(res.result).toEqual(basket)
    })

    it('should respond with a serialized basket when several products are added to an existing basket', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      await addProductstoDatabase(90)
      await addBasketToDatabase(1, 4, gogeta.user.id)
      const basket = toTestBasket(1, 7, gogeta.user.id)

      // when
      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/basket/products/add',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          products: [{
            id: '4',
            quantity: 4
          },
          {
            id: '6',
            quantity: 6
          },
          {
            id: '5',
            quantity: 5
          }]
        }
      })

      // then
      expect(res.statusCode).toEqual(200)
      expect(res.result).toEqual(basket)
    })

    it('should respond with a serialized basket when duplicates products are added to an existing basket', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      await addProductstoDatabase(90)
      await addBasketToDatabase(1, 4, gogeta.user.id)
      const basket = toTestBasket(1, 6, gogeta.user.id)
      basket.basketElements[4].setQuantity(16)
      basket.updateTotalPriceAndQuantity()

      // when
      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/basket/products/add',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          products: [{
            id: '4',
            quantity: 4
          },
          {
            id: '5',
            quantity: 11
          },
          {
            id: '5',
            quantity: 5
          }]
        }
      })

      // then
      expect(res.statusCode).toEqual(200)
      expect(res.result).toEqual(basket)
    })

    it('should respond with 404 without a basket when an only one product doesn\'t match', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      await addProductstoDatabase(90)

      // when
      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/basket/products/add',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          products: [{
            id: 'wrongId',
            quantity: 1
          }]
        }
      })

      // then
      expect(res.statusCode).toEqual(404)
    })

    it('should respond with 404 without a basket when only one product, among others, doesn\'t match', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      await addProductstoDatabase(90)

      // when
      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/basket/products/add',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          products: [{
            id: '1',
            quantity: 1
          },
          {
            id: 'wrongId',
            quantity: 1
          },
          {
            id: '2',
            quantity: 2
          }]
        }
      })

      // then
      expect(res.statusCode).toEqual(404)
    })

    it('should respond with 400 when too much products are added in an order', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      await addProductstoDatabase(90)
      await addBasketToDatabase(1, 4, gogeta.user.id)

      // when
      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/basket/products/add',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          products: [{
            id: '4',
            quantity: 4
          },
          {
            id: '5',
            quantity: 600
          },
          {
            id: '5',
            quantity: 400
          }]
        }
      })

      // then
      expect(res.statusCode).toEqual(400)
    })
  })

  describe('PUT /api/v1/baskets/products/set', () => {
    it('should respond with a serialized updated basket when token is ok', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      await addProductstoDatabase(10)
      await addBasketToDatabase(1, 5, gogeta.user.id)
      const basket = toTestBasket(1, 5, gogeta.user.id)
      basket.basketElements[3].setQuantity(2)
      basket.updateTotalPriceAndQuantity()

      // when
      const res = await server.inject({
        method: 'PUT',
        url: '/api/v1/basket/products/set',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          products: [{
            id: '4',
            quantity: 2
          }]
        }
      })

      // then
      expect(res.statusCode).toEqual(200)
      expect(res.result).toEqual(basket)
    })

    it('should respond with a serialized updated basket when deleting a product which quantity equals 0', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      await addProductstoDatabase(10)
      await addBasketToDatabase(1, 5, gogeta.user.id)
      const basket = toTestBasket(1, 4, gogeta.user.id)

      // when
      const res = await server.inject({
        method: 'PUT',
        url: '/api/v1/basket/products/set',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          products: [{
            id: '4',
            quantity: 0
          }]
        }
      })

      // then
      expect(res.statusCode).toEqual(200)
      expect(res.result).toEqual(basket)
    })

    it('should respond with the same serialized updated basket if a request is send several times', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      await addProductstoDatabase(10)
      await addBasketToDatabase(1, 5, gogeta.user.id)
      const basket = toTestBasket(1, 5, gogeta.user.id)
      basket.basketElements[3].setQuantity(2)
      basket.updateTotalPriceAndQuantity()

      // when
      const res = await server.inject({
        method: 'PUT',
        url: '/api/v1/basket/products/set',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          products: [{
            id: '4',
            quantity: 2
          }]
        }
      })

      const res2 = await server.inject({
        method: 'PUT',
        url: '/api/v1/basket/products/set',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          products: [{
            id: '4',
            quantity: 2
          }]
        }
      })

      // then
      expect(res.statusCode).toEqual(200)
      expect(res.result).toEqual(basket)
      expect(res2.statusCode).toEqual(200)
      expect(res2.result).toEqual(basket)
    })
  })

  describe('PUT /api/v1/baskets/products/delete', () => {
    it('should respond with a empty basket when token is ok', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      await addProductstoDatabase(10)
      await addBasketToDatabase(1, 5, gogeta.user.id)

      // when
      const res = await server.inject({
        method: 'PUT',
        url: '/api/v1/basket/products/delete',
        headers: { authorization: `Bearer ${gogeta.token}` }
      })

      // then
      expect(res.statusCode).toEqual(200)
    })
  })

  describe('PUT /api/v1/order/address', () => {
    it('should respond with a order when token, deliveryAddress and billingAddress is ok', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      const deliveryAddress: Address = new Address('1 rue du palmier', '78784', 'Ile du palmier')
      const billingAddress: Address = new Address('2 rue du palmier', '78784', 'Ile du palmier')
      const order = toTestDatabaseOrder(gogeta.user.id, deliveryAddress, billingAddress, gogeta.user.id)
      await addDatabaseOrderToDatabase(order)
      const expectedResult = new DatabaseOrder('1', deliveryAddress, '1', billingAddress)

      // when
      const res = await server.inject({
        method: 'PUT',
        url: '/api/v1/order/address',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: { deliveryAddress, billingAddress }
      })

      // then
      expect(res.result).toEqual(expectedResult)
    })

    it('should respond throw an error when deliveryAddress or billingAddress do not exist', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      const order = toTestDatabaseOrder(gogeta.user.id)
      await addDatabaseOrderToDatabase(order)

      const payloadBillingAddress: Address = new Address('2 rue du palmier', '78784', 'Ile du palmier')
      const payloadDeliveryAddress: Address = new Address('3 rue du palmier', '78784', 'Ile du palmier')

      // when
      const res = await server.inject({
        method: 'PUT',
        url: '/api/v1/order/address',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: { deliveryAddress: payloadDeliveryAddress, billingAddress: payloadBillingAddress }
      })

      // then
      expect(res.statusCode).toEqual(400)
    })
  })

  describe('GET /api/v1/order', () => {
    it('should respond with an order with an empty basket and no address when token is ok', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      const order = toTestOrder(gogeta.user.id)

      // when
      const res = await server.inject({
        method: 'GET',
        url: '/api/v1/order',
        headers: { authorization: `Bearer ${gogeta.token}` }
      })

      // then
      expect(res.statusCode).toEqual(200)
      expect(res.result.status).toEqual(0)
      expect(res.result.order).toEqual(order)
    })

    it('should respond with an order with a basket when token is ok', async () => {
      // given
      const gogeta = await addGogetaUserToDatabase()
      await addBasketToDatabase(1, 4, gogeta.user.id)
      await addProductstoDatabase(10)
      const deliveryAddresses: Address = new Address('1 rue du palmier', '78784', 'Ile du palmier')
      const billingAddresses: Address = new Address('2 rue du palmier', '78784', 'Ile du palmier')
      const order = toTestDatabaseOrder(gogeta.user.id, deliveryAddresses, billingAddresses, gogeta.user.id)
      await addDatabaseOrderToDatabase(order)

      const targetBasket = toTestBasket(1, 4, gogeta.user.id)
      const targetOrder = toTestOrder(gogeta.user.id, deliveryAddresses, billingAddresses, targetBasket)

      // when
      const res = await server.inject({
        method: 'GET',
        url: '/api/v1/order',
        headers: { authorization: `Bearer ${gogeta.token}` }
      })

      // then
      expect(res.statusCode).toEqual(200)
      expect(res.result.status).toEqual(0)
      expect(res.result.order).toEqual(targetOrder)
    })
  })

  describe('GET /api/v1/order/history', () => {
    it('should respond status with an order history when payment is SUCCEEDED', async () => {
      // given
      const boo = await addBooUserWithMangoPayAttributesToDatabase()
      await addProductstoDatabase(10)
      await addBasketToDatabase(1, 5, boo.user.id)
      const order = toTestOrder(boo.user.id, null, null, toTestBasket(1, 5, boo.user.id))

      // Parse the order to put description to null
      for (const element of order.basket.basketElements) {
        element.product.description = null
      }

      await addOrderToDatabase(order)

      // when
      const res1 = await server.inject({
        method: 'POST',
        url: '/api/v1/mangopay/payin/card/direct',
        headers: { authorization: `Bearer ${boo.token}` },
        payload: {
          cardId: '81925645'
        }
      })

      const res2 = await server.inject({
        method: 'GET',
        url: '/api/v1/order/history',
        headers: { authorization: `Bearer ${boo.token}` }
      })

      expect(res1.result.mpStatus).toEqual('SUCCEEDED')
      expect(res1.result.resultCode).toEqual('000000')

      expect(res2.result.status).toEqual(0)
      expect(res2.result.orderHistory).toBeTruthy()
      expect(res2.result.orderHistory[0].order).toEqual(order)
    })
  })

  describe('GET /api/v1/order/history/{id}', () => {
    it('should respond status with an order history when id is right', async () => {
      // given
      const boo = await addBooUserWithMangoPayAttributesToDatabase()
      await addProductstoDatabase(10)
      await addBasketToDatabase(1, 5, boo.user.id)
      const order = toTestOrder(boo.user.id, null, null, toTestBasket(1, 5, boo.user.id))

      // Parse the order to put description to null
      for (const element of order.basket.basketElements) {
        element.product.description = null
      }

      await addOrderToDatabase(order)

      // when
      const res1 = await server.inject({
        method: 'POST',
        url: '/api/v1/mangopay/payin/card/direct',
        headers: { authorization: `Bearer ${boo.token}` },
        payload: {
          cardId: '81925645'
        }
      })

      const res2 = await server.inject({
        method: 'GET',
        url: '/api/v1/order/history',
        headers: { authorization: `Bearer ${boo.token}` }
      })

      const res3 = await server.inject({
        method: 'GET',
        url: `/api/v1/order/history/${res2.result.orderHistory[0].id}`,
        headers: { authorization: `Bearer ${boo.token}` }
      })

      expect(res1.result.mpStatus).toEqual('SUCCEEDED')
      expect(res1.result.resultCode).toEqual('000000')

      expect(res2.result.status).toEqual(0)
      expect(res2.result.orderHistory).toBeTruthy()
      expect(res2.result.orderHistory[0].order).toEqual(order)

      expect(res3.result.status).toEqual(0)
      expect(res3.result.orderHistory).toBeTruthy()
      expect(res3.result.orderHistory.order.basket).toBeTruthy()
      expect(res3.result.orderHistory.id).toEqual(res2.result.orderHistory[0].id)
    })
  })
})
