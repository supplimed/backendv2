'use strict'
import * as sinon from 'sinon'
import { createServer } from '../../lib/server'
import { DatabaseAlreadyEmptyError } from '../../db/filesystem/lib/database-errors'
import { fsd as filesystemDatabase } from '../../db/filesystem'
import { addGogetaUserToDatabase } from '../test-utils'
import { User } from '../../lib/domain/models/User'
import { Address } from '../../lib/domain/models/Address'
import { AddressAlreadySaved, AddressIsEmpty } from '../../lib/domain/errors'

describe('User', () => {
  let server

  beforeEach(async () => {
    try {
      await filesystemDatabase.emptyDatabase()
    } catch (err) {
      if (!(err instanceof DatabaseAlreadyEmptyError)) {
        throw err
      }
    }
    server = await createServer()
  })

  afterEach(async () => {
    await server.stop()
    sinon.restore()
  })

  describe('GET /api/v1/users/me', () => {
    it('should respond with a serialized user profile when token is ok', async () => {
      // when
      const gogeta = await addGogetaUserToDatabase()
      const domainUser: User = User.fromDatabaseUser(gogeta.user)

      const res = await server.inject({
        method: 'GET',
        url: '/api/v1/users/me',
        headers: { authorization: `Bearer ${gogeta.token}` }
      })

      // then
      expect(res.statusCode).toEqual(200)
      expect(res.result.data).toEqual(domainUser)
    })
    it('should respond with 401 unauthorized when the token is not valid', async () => {
      // when
      const gogeta = await addGogetaUserToDatabase()

      const res = await server.inject({
        method: 'GET',
        url: '/api/v1/users/me',
        headers: { authorization: `Bearerrrrrr ${gogeta.token}` }
      })

      // then
      expect(res.statusCode).toEqual(401)
      expect(res.result).toEqual({ msg: 'Invalid token' })
    })
  })

  describe('PATCH /api/v1/users/{id}', () => {
    it('should respond with a serialized updated user profile when token is ok', async () => {
      // when
      const gogeta = await addGogetaUserToDatabase()
      const domainUser: User = User.fromDatabaseUser(gogeta.user)
      domainUser.email = 'gogeta@namek.com'

      const res = await server.inject({
        method: 'PATCH',
        url: `/api/v1/users/${gogeta.user.id}`,
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          newEmail: 'gogeta@namek.com',
          newEmailConfirmation: 'gogeta@namek.com'
        }
      })

      // then
      expect(res.statusCode).toEqual(200)
      expect(res.result.data).toEqual(domainUser)
    })
    it('should respond with a 400 when the new email has the wrong format', async () => {
      // when
      const gogeta = await addGogetaUserToDatabase()
      const emailWithWrongFormat = 'gogeta@namek'

      const res = await server.inject({
        method: 'PATCH',
        url: `/api/v1/users/${gogeta.user.id}`,
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          newEmail: emailWithWrongFormat,
          newEmailConfirmation: emailWithWrongFormat
        }
      })

      // then
      expect(res.statusCode).toEqual(400)
      expect(res.result).toEqual({ msg: `New email (${emailWithWrongFormat}) does not look like an email.` })
    })
  })

  describe('PUT /api/v1/users/{id}/password', () => {
    it('should respond with 204 when the password update is successful and respond with 200 when trying to login with updated user', async () => {
      // when
      const gogeta = await addGogetaUserToDatabase()
      const updatedPassword = 'supersayan2'

      const res = await server.inject({
        method: 'PUT',
        url: `/api/v1/users/${gogeta.user.id}/password`,
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          oldPassword: 'supersayan',
          newPassword: updatedPassword,
          newPasswordConfirmation: updatedPassword
        }
      })

      const res2 = await server.inject({
        method: 'post',
        url: '/authentication/login',
        payload: {
          email: 'gogeta.denamek@dbz.com',
          password: updatedPassword
        }
      })

      // then
      expect(res.statusCode).toEqual(204)
      expect(res2.statusCode).toEqual(200)
    })
  })

  describe('POST /api/v1/users/address/add', () => {
    it('should respond a user with an updated deliveryAddresses', async () => {
      // when
      const gogeta = await addGogetaUserToDatabase()
      const address: Address = new Address('3 rue du palmier', '78784', 'Ile du palmier')
      const addresses: Address[] = [new Address('1 rue du palmier', '78784', 'Ile du palmier'), new Address('2 rue du palmier', '78784', 'Ile du palmier'), address]
      const user: User = new User('1', 'gogeta.denamek@dbz.com', 'Gogeta', 'DeNamek', null, null, null, addresses)

      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/users/address/add',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          address: address
        }
      })

      // then
      expect(res.result).toEqual(user)
    })

    it('should respond with an error when deliveryAddress and billingAddress are null ', async () => {
      // when
      const gogeta = await addGogetaUserToDatabase()

      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/users/address/add',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          address: null
        }
      })

      // then
      expect(res.result.errorDescription).toEqual(new AddressIsEmpty().message)
    })

    it('should respond with an error when the updated address is already saved', async () => {
      // when
      const gogeta = await addGogetaUserToDatabase()
      const address: Address = new Address('1 rue du palmier', '78784', 'Ile du palmier')

      const res = await server.inject({
        method: 'POST',
        url: '/api/v1/users/address/add',
        headers: { authorization: `Bearer ${gogeta.token}` },
        payload: {
          address: address
        }
      })

      // then
      expect(res.result.errorDescription).toEqual(new AddressAlreadySaved().message)
    })
  })
})
