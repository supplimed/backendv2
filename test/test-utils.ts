import { Product } from '../lib/domain/models/Product'
import { Basket } from '../lib/domain/models/Basket'
import { BasketElement } from '../lib/domain/models/BasketElement'
import { DatabaseBasketElement } from '../db/filesystem/models/DatabaseBasketElement'
import { DatabaseBasket } from '../db/filesystem/models/DatabaseBasket'
import * as _ from 'lodash'
import * as bcrypt from 'bcrypt'
import * as securityService from '../lib/domain/services/security-service'
import { fsd as filesystemDatabase } from '../db/filesystem'
import { DatabaseUser } from '../db/filesystem/models/DatabaseUser'
import { DatabaseProduct } from '../db/filesystem/models/DatabaseProduct'
import { User } from '../lib/domain/models/User'
import { Order } from '../lib/domain/models/Order'
import { Address } from '../lib/domain/models/Address'
import { DatabaseOrder } from '../db/filesystem/models/DatabaseOrder'

interface GogetaI {
  user: DatabaseUser
  token: string
}

function _toTestDatabaseBasketElement (databaseBasketElement: DatabaseBasketElement): DatabaseBasketElement {
  return new DatabaseBasketElement(databaseBasketElement.productId, databaseBasketElement.quantity || parseInt(databaseBasketElement.productId, 10))
}

export async function addBasketToDatabase (start: number, stop: number, id: string): Promise<DatabaseBasket> {
  const basketDatabaseElements = _.range(start, stop).map(i => ({ productId: `${i}`, quantity: i })).map(_toTestDatabaseBasketElement)
  const databaseBasket = new DatabaseBasket(id, basketDatabaseElements)
  await filesystemDatabase.addBasket(databaseBasket)
  return databaseBasket
}

export async function addGogetaUserToDatabase (): Promise<GogetaI> {
  const currentPasswordSalt = 10
  const addresses: Address[] = [new Address('1 rue du palmier', '78784', 'Ile du palmier'), new Address('2 rue du palmier', '78784', 'Ile du palmier')]
  const user: DatabaseUser = new DatabaseUser('1', 'gogeta.denamek@dbz.com', bcrypt.hashSync('supersayan', currentPasswordSalt), 'Gogeta', 'DeNamek', null, null, null, addresses)
  const token = securityService.createTokenFromUser(user)
  await filesystemDatabase.addUser(user)
  return {
    user,
    token
  }
}

export async function addGogetaUserWithMangoPayAttributesToDatabase (): Promise<GogetaI> {
  const currentPasswordSalt = 10
  const user: DatabaseUser = new DatabaseUser('1', 'gogeta.denamek@dbz.com', bcrypt.hashSync('supersayan', currentPasswordSalt), 'Gogeta', 'DeNamek', '81455971', '', ['81455972'])
  const token = securityService.createTokenFromUser(user)
  await filesystemDatabase.addUser(user)
  return {
    user,
    token
  }
}

export async function addBooUserWithMangoPayAttributesToDatabase (): Promise<GogetaI> {
  const currentPasswordSalt = 10
  const user: DatabaseUser = new DatabaseUser('2', 'booboo@dbz.com', bcrypt.hashSync('babidi boo', currentPasswordSalt), 'Boo', 'Boo', '80843674', '80843675', ['81925645'])
  const token = securityService.createTokenFromUser(user)
  await filesystemDatabase.addUser(user)
  return {
    user,
    token
  }
}

export async function addGohanUserWithMangoPayAttributesToDatabase (): Promise<GogetaI> {
  const currentPasswordSalt = 10
  const user: DatabaseUser = new DatabaseUser('3', 'gohan@dbz.com', bcrypt.hashSync('chichi', currentPasswordSalt), 'Gohan', 'Son', '82007131', null, null)
  const token = securityService.createTokenFromUser(user)
  await filesystemDatabase.addUser(user)
  return {
    user,
    token
  }
}

export async function addOrderToDatabase (order: Order): Promise<Order> {
  await filesystemDatabase.addOrder(DatabaseOrder.fromOrder(order))
  return order
}
export async function addDatabaseOrderToDatabase (databaseOrder: DatabaseOrder): Promise<DatabaseOrder> {
  await filesystemDatabase.addOrder(databaseOrder)
  return databaseOrder
}

export async function addProductstoDatabase (range: number): Promise<DatabaseProduct[]> {
  const products = _.range(range).map(i => ({ id: `${i}` })).map(toTestProduct)
  await filesystemDatabase.addProducts(products)
  return products
}

export function createGogetaDomainUser (): User {
  return new User('1', 'gogeta.denamek@dbz.com', 'Gogeta', 'DeNamek')
}

export function getSliceOfSerializedProduct (products: DatabaseProduct[], start: number, end: number): Product[] {
  return _.slice(products, start, end).map(p => _.merge(_.clone(p), { type: 'product' }))
}

export async function getUserById (userid: string): Promise<DatabaseUser> {
  return await filesystemDatabase.getItemById('users', userid)
}

// Create a basket with n = 'stop minus start' products
export function toTestBasket (start: number, stop: number, id: string): Basket {
  const basketElements = _.range(start, stop).map(i => ({ product: { id: `${i}` } })).map(toTestBasketElement)
  return new Basket(id, basketElements)
}

export function toTestBasketElement (basketElement: BasketElement): BasketElement {
  const product: Product = toTestProduct({ currency: undefined, description: '', imageUrl: '', name: '', price: 0, id: basketElement.product.id })
  return new BasketElement(product, basketElement.quantity || parseInt(basketElement.product.id))
}

export function toTestOrder (userId: string, deliveryAddress?: Address, billingAddress?: Address, basket?: Basket): Order {
  return new Order(userId, basket || new Basket(userId, []), deliveryAddress || null, billingAddress || null)
}

export function toTestDatabaseOrder (userId: string, deliveryAddress?: Address, billingAddress?: Address, basketId?: string): DatabaseOrder {
  return new DatabaseOrder(userId, deliveryAddress || null, basketId || null, billingAddress || null)
}

export function toTestProduct (product: Product): Product {
  return new Product(
    product.id,
    product.name || `Name for ${product.id}`,
    product.price || parseInt(product.id, 10),
    product.description || `Description for ${product.id}`,
    product.imageUrl || 'https://fake-img-server.com/url.jpg'
  )
}
